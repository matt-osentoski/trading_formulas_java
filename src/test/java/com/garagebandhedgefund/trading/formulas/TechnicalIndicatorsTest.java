package com.garagebandhedgefund.trading.formulas;

import com.garagebandhedgefund.trading.model.StockPrice;
import com.garagebandhedgefund.trading.vo.BollingerBandVO;
import com.garagebandhedgefund.trading.vo.StochasticVO;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 8/22/14
 * Time: 3:32 PM
 */
public class TechnicalIndicatorsTest {
    @Test
    public void testGetPivotPoint() throws Exception {

    }

    @Test
    public void testGetSMA() throws Exception {
        double[] priceArr = new double[] {59.375, 58.9062, 54.3125, 51, 51.5938, 52, 55.4375,
                52.9375, 54.5312, 56.5312, 55.3438, 55.7188, 50.1562};
        List<StockPrice> prices = new ArrayList<StockPrice>();
        for (int x = 0; x<priceArr.length; x++) {
            StockPrice sp = new StockPrice();
            sp.setClosingPrice(priceArr[x]);
            prices.add(sp);
        }

        double testPrice = TechnicalIndicators.getSMA(prices, 6, 3);
        assertEquals(53.01043, testPrice, 0.00001);
        assertNotEquals(53.01045, testPrice, 0.00001);
    }

    @Test
    public void testGetEMA() throws Exception {
        double[] priceArr = new double[] {64.75, 63.79, 63.73, 63.73, 63.55, 63.19, 63.91, 63.85,
                62.95, 63.37, 61.33, 61.51, 61.87, 60.25, 59.35, 59.95,
                58.93, 57.68, 58.82, 58.87};
        List<StockPrice> prices = new ArrayList<StockPrice>();
        for (double price : priceArr) {
            StockPrice sp = new StockPrice();
            sp.setClosingPrice(price);
            prices.add(sp);
        }
        List<Double> emas = new ArrayList<Double>();
        double prevEma = 0;
        for (int x = 0; x < prices.size(); x++)
        {
            double tmpEma = TechnicalIndicators.getEMA(prices, x, 10, prevEma);
            prevEma = tmpEma;
            emas.add(tmpEma);
        }
        assertEquals(61.755, emas.get(14), 0.001);
        assertNotEquals(61.855, emas.get(14), 0.001);
    }

    @Test
    public void testGetMACD() throws Exception {
        double ema12 = 29.24F;
        double ema26 = 28.21F;
        double macd = TechnicalIndicators.getMACD(ema12, ema26);
        assertEquals(1.03, macd, 0.01);
        assertNotEquals(1.05, macd, 0.01);
    }

    @Test
    public void testGetMACDEMA() throws Exception {
        //TODO This test has to be rethought from the .NET version of the test.
    }

    @Test
    public void testGetMACDHistogram() throws Exception {
        double macd = 1.01F;
        double ema9 = 0.88F;
        double macdHist = TechnicalIndicators.getMACDHistogram(macd, ema9);
        assertEquals(0.13, macdHist, 0.01);
        assertNotEquals(0.14, macdHist, 0.01);
    }

    @Test
    public void testGetVolSMA() throws Exception {
        int[] volArr = new int[] {2981600, 2346400, 5444000, 4773600, 7429600, 8826400,
                6886400, 3495200, 3816000, 2387200, 3591200, 2833600,
                5977600, 3987200, 3796000, 8344800, 3521600};
        List<StockPrice> prices = new ArrayList<StockPrice>();
        for (int vol : volArr) {
            StockPrice sp = new StockPrice();
            sp.setVolume(vol);
            prices.add(sp);
        }
        long testPrice = TechnicalIndicators.getVolSMA(prices, 6, 3);
        assertEquals(7714133, testPrice);
        assertNotEquals(7714134, testPrice);
    }

    @Test
    public void testGetPPO() throws Exception {
        double ema12 = 227.69F;
        double ema26 = 221.28F;
        double ppo = TechnicalIndicators.getPPO(ema12, ema26);
        assertEquals(0.02897, ppo, 0.00001);
        assertNotEquals(0.02899, ppo, 0.00001);
    }

    @Test
    public void testGetPPOEMA() throws Exception {
        //TODO This test has to be rethought from the .NET version of the test.
    }

    @Test
    public void testGetPPOHistogram() throws Exception {
        double ppo = 0.03158;
        double ema9 = 0.02832;
        ppo = TechnicalIndicators.getPPOHistogram(ppo, ema9);
        assertEquals(0.00326, ppo, 0.00001);
        assertNotEquals(0.00327, ppo, 0.00001);
    }

    @Test
    public void testGetRSI() throws Exception {
        double[] priceArr = new double[] {46.1250, 47.1250, 46.4375, 46.9375, 44.9375, 44.2500,
                44.6250, 45.7500, 47.8125, 47.5625, 47.0000, 44.5625,
                46.3125, 47.6875, 46.6875, 45.6875, 43.0625, 43.5625,
                44.8750, 43.6875};
        List<StockPrice> prices = new ArrayList<StockPrice>();
        for (double price : priceArr) {
            StockPrice sp = new StockPrice();
            sp.setClosingPrice(price);
            prices.add(sp);
        }
        double rsi = TechnicalIndicators.getRSI(prices, 19, 14);
        assertEquals(43.9921, rsi, 0.0001);
        assertNotEquals(43.9920, rsi, 0.0001);
    }

    @Test
    public void testGetStochastics() throws Exception {
        double[] highArr = new double[] {421.89, 421.4, 421.62, 418.85, 420.35, 414.85, 411.64,
                413.61, 415.83, 414.95, 415.29, 416.07, 418.28, 420.31,
                418.62, 417.18, 416.44, 420.52, 420.58, 425.27, 425.22,
                422.44, 421.43};
        double[] lowArr = new double[] {419.44, 419.78, 418.19, 416.93, 413.58, 410.7, 408.3,
                410.53, 413.51, 413.38, 413.76, 413.35, 415.31, 417.49,
                416.76, 414.3, 414.44, 416.34, 419.13, 419.58, 419.54,
                417.77, 419.62};
        double[] closeArr = new double[] {420.74, 421.34, 418.19, 418.26, 414.85, 410.72, 411.61,
                413.51, 413.53, 414.84, 414.03, 416.07, 417.98, 417.98,
                417.08, 414.44, 416.36, 419.95, 419.58, 425.27, 419.77,
                419.92, 419.93};
        List<StockPrice> prices = new ArrayList<StockPrice>();
        for (int x = 0; x<highArr.length; x++) {
            StockPrice sp = new StockPrice();
            sp.setHighPrice(highArr[x]);
            sp.setLowPrice(lowArr[x]);
            sp.setClosingPrice(closeArr[x]);
            prices.add(sp);
        }
        StochasticVO stoch = TechnicalIndicators.getStochastics(prices, 22, 10, 3);
        assertEquals(51.32, stoch.getK(), 0.01);
        assertEquals(52.14, stoch.getD(), 0.01);
        assertEquals(52.14, stoch.getkSlow(), 0.01);
        assertEquals(66.84, stoch.getdSlow(), 0.01);
        assertNotEquals(51.31, stoch.getK(), 0.01);
        assertNotEquals(52.15, stoch.getD(), 0.01);
        assertNotEquals(52.15, stoch.getkSlow(), 0.01);
        assertNotEquals(66.85, stoch.getdSlow(), 0.01);
    }

    @Test
    public void testGetATR() throws Exception {
        double[] highArr = new double[] {61, 61, 58.8438, 55.125, 54.0625, 53.9688, 56, 55.2188,
                55.0312, 57.4922, 57.0938, 56.8125, 55.5625, 50.0625,
                47.6875, 44.9062, 47.375, 47.625, 48.0156, 45.0391, 43.75,
                43.25, 43, 42.5, 44.875, 44.3125, 41.2188, 39.375, 40.9375,
                40.5938, 46, 48.125, 45};
        double[] lowArr = new double[] {59.0312, 58.375, 53.625, 47.4375, 50.5, 49.7812, 52.5,
                52.625, 53.25, 53.75, 55.25, 54.3438, 50, 46.8438, 44.4688,
                40.625, 44.1641, 45.125, 43.25, 42.6875, 40.75, 39.9688, 40,
                40.75, 43.375, 40.0625, 37.625, 36.5, 37.5625, 36.9375,
                40.8438, 42.5625, 42.5};
        double[] closeArr = new double[] {59.375, 58.9062, 54.3125, 51, 51.5938, 52, 55.4375,
                52.9375, 54.5312, 56.5312, 55.3438, 55.7188, 50.1562,
                48.8125, 44.5938, 42.6562, 47, 46.9688, 43.625, 44.6562,
                40.8125, 42.5625, 40, 42.4375, 44.0938, 40.625, 39.875,
                38.0312, 38.4688, 39.4375, 45.875, 44.25, 42.8125};
        List<StockPrice> prices = new ArrayList<StockPrice>();

        for (int x = 0; x<highArr.length; x++)
        {
            StockPrice sp = new StockPrice();
            sp.setHighPrice(highArr[x]);
            sp.setLowPrice(lowArr[x]);
            sp.setClosingPrice(closeArr[x]);
            prices.add(sp);
        }

        double prevAtr = 0;
        List<Double> atrResults = new ArrayList<Double>();
        for (int x = 0; x<prices.size(); x++)
        {
            double atr = TechnicalIndicators.getATR(prices, x, prevAtr, 14);
            prevAtr = atr;
            atrResults.add(atr);
        }
        assertEquals(3.77148, atrResults.get(32), 0.00001);
        assertNotEquals(3.77147, atrResults.get(32), 0.00001);
    }

    @Test
    public void testGetBollingerBands() throws Exception {
        double[] closeArr = new double[] {
                86.16, 89.09, 88.78, 90.32, 89.07, 91.15, 89.44, 89.18, 86.93, 87.68, 86.96, 89.43, 89.32, 88.72, 87.45,
                87.26, 89.50, 87.90, 89.13, 90.70, 92.90, 92.98, 91.80, 92.66, 92.68, 92.30, 92.77, 92.54, 92.95, 93.20,
                91.07, 89.83, 89.74, 90.40, 90.74, 88.02, 88.09, 88.84, 90.78, 90.54, 91.39, 90.65
        };
        List<StockPrice> prices = new ArrayList<StockPrice>();

        for (int x = 0; x<closeArr.length; x++)
        {
            StockPrice sp = new StockPrice();
            sp.setClosingPrice(closeArr[x]);
            prices.add(sp);
        }
        BollingerBandVO bb = TechnicalIndicators.getBollingerBands(prices, 30, 20, 2);
        assertNotNull(prices);
        assertNotNull(bb);
        assertEquals(94.91, bb.getUpperBand(), 0.01);
        assertEquals(86.82, bb.getLowerBand(), 0.01);
        assertEquals(8.09, bb.getBandwidth(), 0.01);
        // negative test
        assertNotEquals(94.70, bb.getUpperBand(), 0.01);
        assertNotEquals(86.92, bb.getLowerBand(), 0.01);
        assertNotEquals(8.19, bb.getBandwidth(), 0.01);

        bb = TechnicalIndicators.getBollingerBands(prices, 39, 20, 2);
        assertEquals(94.53, bb.getUpperBand(), 0.01);
        assertEquals(87.95, bb.getLowerBand(), 0.01);
        assertEquals(6.58, bb.getBandwidth(), 0.01);
    }

    @Test
    public void testGetLinearRegressionSlope() throws Exception {
        double[] bullCloseArr = new double[] {31.65, 31.97, 32.25, 32.28, 34.62,
                34.48, 32.28, 32.73, 34.9, 35.1, 35.33, 34.94, 35.23,
                35.24, 35.38, 36.03, 36.12, 36.32, 36.99, 38.45, 38.76,
                39.81, 38.9, 39.42, 39.47, 40.45, 39.37, 39.18, 40.6, 42.31};
        double[] bearCloseArr = new double[] {51.65, 51.97, 52.25, 49.28, 48.62,
                49.48, 51.28, 48.73, 48.9, 48.1, 47.33, 46.94, 46.23,
                44.24, 45.38, 44.03, 43.12, 43.32, 42.99, 42.45, 43.76,
                42.81, 41.9, 39.42, 39.47, 38.45, 39.37, 38.18, 37.6, 36.31};
        double[] sidewaysCloseArr = new double[] {31.97, 32.25, 32.28, 34.62, 31.65, 31.97,
                31.97, 32.25, 32.28, 34.62, 31.65, 31.97, 31.97, 32.25, 32.28, 34.62, 31.65, 31.97,
                31.97, 32.25, 32.28, 34.62, 31.65, 31.97, 31.97, 32.25, 32.28, 34.62, 31.65, 31.97,
                31.97, 32.25, 32.28, 34.62, 31.65, 31.97};

        List<StockPrice> bullPrices = new ArrayList<StockPrice>();
        List<StockPrice> bearPrices = new ArrayList<StockPrice>();
        List<StockPrice> sidewaysPrices = new ArrayList<StockPrice>();
        for (int x = 0; x < bullCloseArr.length; x++)
        {
            StockPrice bullPrice = new StockPrice();
            bullPrice.setClosingPrice(bullCloseArr[x]);
            bullPrices.add(bullPrice);

            StockPrice bearPrice = new StockPrice();
            bearPrice.setClosingPrice(bearCloseArr[x]);
            bearPrices.add(bearPrice);

            StockPrice sidewaysPrice = new StockPrice();
            sidewaysPrice.setClosingPrice(sidewaysCloseArr[x]);
            sidewaysPrices.add(sidewaysPrice);
        }

        double bullSlope = TechnicalIndicators.getLinearRegressionSlope(bullPrices, 30);
        double bull = bullSlope *100;
        assertTrue(bullSlope > 0);
        double bearSlope = TechnicalIndicators.getLinearRegressionSlope(bearPrices, 30);
        double bear = bearSlope *100;
        assertTrue(bearSlope < 0);
        double sidewaysSlope = TechnicalIndicators.getLinearRegressionSlope(sidewaysPrices, 30);
        double sideways = sidewaysSlope * 100;
        assertEquals(0, sidewaysSlope, 0.001);
    }

    @Test
    public void testGetHistoricalVolatility() throws Exception {
        double[] closeArr = new double[] {31.65, 31.97, 32.25, 32.28, 34.62,
                34.48, 32.28, 32.73, 34.9, 35.1, 35.33, 34.94, 35.23,
                35.24, 35.38, 36.03, 36.12, 36.32, 36.99, 38.45, 38.76,
                39.81, 38.9, 39.42, 39.47, 40.45, 39.37, 39.18, 40.6, 42.31};
        List<StockPrice> prices = new ArrayList<StockPrice>();
        for (int x = 0; x < closeArr.length; x++)
        {
            StockPrice sp = new StockPrice();
            sp.setClosingPrice(closeArr[x]);
            prices.add(sp);
        }
        double vola = TechnicalIndicators.getHistoricalVolatility(prices, 28, 10, 252);
        assertEquals(0.364606, vola, 0.000001);
        assertNotEquals(0.364605, vola, 0.000001);
    }

    @Test
    public void testGetEWMAVolatility() throws Exception {
        double[] closeArr = new double[] {100.0, 105.0, 110.0, 100.0, 110.0, 120.0, 100.0, 125.0,
                90.0, 130.0, 85.0, 140.0};
        List<StockPrice> prices = new ArrayList<StockPrice>();
        for (int x = 0; x < closeArr.length; x++)
        {
            StockPrice sp = new StockPrice();
            sp.setClosingPrice(closeArr[x]);
            prices.add(sp);
        }
        double vola = TechnicalIndicators.getEWMAVolatility(prices, 11, 1, 0.90, 252);
        assertEquals(0.21523, vola, 0.00001);
        assertNotEquals(0.21522, vola, 0.00001);
    }

    @Test
    public void testGetGarch1_1Volatility() throws Exception {
        double[] closeArr = new double[] {100.0, 105.0, 110.0, 100.0, 110.0, 120.0, 100.0, 125.0,
                90.0, 130.0, 85.0, 140.0};
        List<StockPrice> prices = new ArrayList<StockPrice>();
        for (int x = 0; x < closeArr.length; x++) {
            StockPrice sp = new StockPrice();
            sp.setClosingPrice(closeArr[x]);
            prices.add(sp);
        }
        double vola = TechnicalIndicators.getGarch1_1Volatility(prices, 11, 0.05, 0.10, 0.80, 0.90, 1, 252);
        assertEquals(0.21951, vola, 0.00001);
        assertNotEquals(0.21952, vola, 0.00001);
    }
}
