package com.garagebandhedgefund.trading.formulas;

import com.garagebandhedgefund.trading.model.StockPrice;
import com.garagebandhedgefund.trading.vo.StopLossVO;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MoneyManagementFormulasTest {

    private List<StockPrice> prices;

    @Before
    public void setUp() throws Exception {
        prices = new ArrayList<StockPrice>();
        double[] lowPriceArr = new double[] {
            33.970001, 32.880001, 31.34, 30.32, 28.959999, 27.809999, 27.620001, 28, 26.9, 25.93, 25.68, 26.23,
            25.549999, 25.75, 26, 26.27, 24.74, 25.209999, 25.01, 26.120001, 26.76
        };
        double[] highPriceArr = new double[] {
            36.330002, 34.169998, 34.52, 31.66, 29.58, 29.32, 29.549999, 29.25, 27.67, 26.66, 26.6, 27.700001,
            26.99, 27.709999, 26.809999, 27.190001, 26.360001, 26.24, 25.98, 30.23, 32.48
        };

        for (int x=0; x<lowPriceArr.length; x++) {
            StockPrice price = new StockPrice();
            price.setSymbol("TVIX");
            price.setLowPrice(lowPriceArr[x]);
            price.setHighPrice(highPriceArr[x]);
            prices.add(price);
        }
    }

    @Test
    public void createStopLoss() {
        StopLossVO stopLoss = MoneyManagementFormulas.createStopLoss(prices, 10000, 35.12);
        assertNotNull(stopLoss);
        assertEquals(119, stopLoss.getShares());
        assertEquals(33.44, stopLoss.getStopLoss(), 0.01);
        assertNotEquals(110, stopLoss.getShares());
        assertNotEquals(33.20, stopLoss.getStopLoss(), 0.01);
    }

    @Test
    public void createStopLossAdvanced() {
        StopLossVO stopLoss = MoneyManagementFormulas.createStopLoss(prices, 10000, 35.12,
                0.05, 10);
        assertNotNull(stopLoss);
        assertEquals(249, stopLoss.getShares());
        assertEquals(33.12, stopLoss.getStopLoss(), 0.01);
        assertNotEquals(225, stopLoss.getShares());
        assertNotEquals(33.05, stopLoss.getStopLoss(), 0.01);
    }

    @Test
    public void getAveragePriceRange() {
        double range = MoneyManagementFormulas.getAveragePriceRange(prices, 20);
        assertNotEquals(0, range);
        assertEquals(1.6795, range, 0.01);
        assertNotEquals(1.0, range, 0.01);
    }

    @Test
    public void maxSharesByAccountBalance() {
        int shares = MoneyManagementFormulas.maxSharesByAccountBalance(10000, 35.00);
        assertNotEquals(0, shares);
        assertEquals(286, shares);
        assertNotEquals(250, shares);
    }
}