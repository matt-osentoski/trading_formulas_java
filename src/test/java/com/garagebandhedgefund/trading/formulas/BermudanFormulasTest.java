package com.garagebandhedgefund.trading.formulas;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class BermudanFormulasTest {

    @Test
    public void testOptionPriceCallBermudanBinomial() throws Exception {
        double S = 80;
        double K = 100;
        double r = 0.20;
        double q = 0.0;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 500;
        double[] potentialExerciseTimes = new double[] { 0.25, 0.5, 0.75 };
        double testVal = BermudanFormulas.OptionPriceCallBermudanBinomial(S, K, r, q, sigma,
                time, potentialExerciseTimes, steps);
        assertEquals(7.14016, testVal, 0.00001);

        //Negative test
        sigma = 0.26;
        testVal = BermudanFormulas.OptionPriceCallBermudanBinomial(S, K, r, q, sigma,
                time, potentialExerciseTimes, steps);
        assertNotEquals(7.14016, testVal, 0.00001);
    }

    @Test
    public void testOptionPricePutBermudanBinomial() throws Exception {
        double S = 80;
        double K = 100;
        double r = 0.20;
        double q = 0.0;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 500;
        double[] potentialExerciseTimes = new double[] { 0.25, 0.5, 0.75 };
        double testVal = BermudanFormulas.OptionPricePutBermudanBinomial(S, K, r, q, sigma,
                time, potentialExerciseTimes, steps);
        assertEquals(15.8869, testVal, 0.0001);

        //Negative test
        sigma = 0.26;
        testVal = BermudanFormulas.OptionPricePutBermudanBinomial(S, K, r, q, sigma,
                time, potentialExerciseTimes, steps);
        assertNotEquals(15.8869, testVal, 0.0001);
    }
}