package com.garagebandhedgefund.trading.formulas;

import com.garagebandhedgefund.trading.model.OptionPartials;
import org.junit.Test;

import static org.junit.Assert.*;

public class BinomialFormulasTest {

    @Test
    public void testOptionPriceCallAmericanBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double testVal = BinomialFormulas.optionPriceCallAmericanBinomial(S, K, r, sigma, time, steps);
        assertEquals(14.9505, testVal, 0.0001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPriceCallAmericanBinomial(S, K, r, sigma, time, steps);
        assertNotEquals(14.9505, testVal, 0.0001);
    }

    @Test
    public void testOptionPricePutAmericanBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double testVal = BinomialFormulas.optionPricePutAmericanBinomial(S, K, r, sigma, time, steps);
        assertEquals(6.54691, testVal, 0.00001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPricePutAmericanBinomial(S, K, r, sigma, time, steps);
        assertNotEquals(6.54691, testVal, 0.00001);
    }

    @Test
    public void testOptionPriceDeltaAmericanCallBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double testVal = BinomialFormulas.optionPriceDeltaAmericanCallBinomial(S, K, r, sigma, time, steps);
        assertEquals(0.699792, testVal, 0.000001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPriceDeltaAmericanCallBinomial(S, K, r, sigma, time, steps);
        assertNotEquals(0.699792, testVal, 0.000001);
    }

    @Test
    public void testOptionPriceDeltaAmericanPutBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double testVal = BinomialFormulas.optionPriceDeltaAmericanPutBinomial(S, K, r, sigma, time, steps);
        assertEquals(-0.387636, testVal, 0.000001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPriceDeltaAmericanPutBinomial(S, K, r, sigma, time, steps);
        assertNotEquals(-0.387636, testVal, 0.000001);
    }

    @Test
    public void testOptionPricePartialsAmericanCallBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;

        OptionPartials partial = BinomialFormulas.optionPricePartialsAmericanCallBinomial(S, K, r, sigma, time, steps);
        assertEquals(0.699792, partial.getDelta(), 0.000001);
        assertEquals(0.0140407, partial.getGamma(), 0.0000001);
        assertEquals(-9.89067, partial.getTheta(), 0.00001);
        assertEquals(34.8536, partial.getVega(), 0.0001);
        assertEquals(56.9652, partial.getRho(), 0.0001);

        //Negative test
        sigma = 0.26;
        partial = BinomialFormulas.optionPricePartialsAmericanCallBinomial(S, K, r, sigma, time, steps);
        assertNotEquals(0.699792, partial.getDelta(), 0.000001);
        assertNotEquals(0.0140407, partial.getGamma(), 0.0000001);
        assertNotEquals(-9.89067, partial.getTheta(), 0.00001);
        assertNotEquals(34.8536, partial.getVega(), 0.0001);
        assertNotEquals(56.9652, partial.getRho(), 0.0001);
    }

    @Test
    public void testOptionPricePartialsAmericanPutBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;

        OptionPartials partial = BinomialFormulas.optionPricePartialsAmericanPutBinomial(S, K, r, sigma, time, steps);
        assertEquals(-0.387636, partial.getDelta(), 0.000001);
        assertEquals(0.0209086, partial.getGamma(), 0.0000001);
        assertEquals(-1.99027, partial.getTheta(), 0.00001);
        assertEquals(35.3943, partial.getVega(), 0.0001);
        assertEquals(-21.5433, partial.getRho(), 0.0001);

        //Negative test
        sigma = 0.26;
        partial = BinomialFormulas.optionPricePartialsAmericanPutBinomial(S, K, r, sigma, time, steps);
        assertNotEquals(-0.387636, partial.getDelta(), 0.000001);
        assertNotEquals(0.0209086, partial.getGamma(), 0.0000001);
        assertNotEquals(-1.99027, partial.getTheta(), 0.00001);
        assertNotEquals(35.3943, partial.getVega(), 0.0001);
        assertNotEquals(-21.5433, partial.getRho(), 0.0001);
    }

    @Test
    public void testOptionPriceCallAmericanDiscreteDividendsBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double[] dividendTimes = new double[] { 0.25, 0.75 };
        double[] dividendAmounts = new double[] { 2.5, 2.5 };
        double testVal = BinomialFormulas.optionPriceCallAmericanDiscreteDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
        assertEquals(12.0233, testVal, 0.0001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPriceCallAmericanDiscreteDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
        assertNotEquals(12.0233, testVal, 0.0001);
    }

    @Test
    public void testOptionPricePutAmericanDiscreteDividendsBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double[] dividendTimes = new double[] { 0.25, 0.75 };
        double[] dividendAmounts = new double[] { 2.5, 2.5 };
        double testVal = BinomialFormulas.optionPricePutAmericanDiscreteDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
        assertEquals(8.11801, testVal, 0.00001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPricePutAmericanDiscreteDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
        assertNotEquals(8.11801, testVal, 0.00001);
    }

    @Test
    public void testOptionPriceCallAmericanProportionalDividendsBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double[] dividendTimes = new double[] { 0.25, 0.75 };
        double[] dividendAmounts = new double[] { 0.025, 0.025 };
        double testVal = BinomialFormulas.optionPriceCallAmericanProportionalDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
        assertEquals(11.8604, testVal, 0.0001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPriceCallAmericanProportionalDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
        assertNotEquals(11.8604, testVal, 0.0001);
    }

    @Test
    public void testOptionPricePutAmericanProportionalDividendsBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double[] dividendTimes = new double[] { 0.25, 0.75 };
        double[] dividendAmounts = new double[] { 0.025, 0.025 };
        double testVal = BinomialFormulas.optionPricePutAmericanProportionalDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
        assertEquals(7.99971, testVal, 0.00001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPricePutAmericanProportionalDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
        assertNotEquals(7.99971, testVal, 0.00001);
    }

    @Test
    public void testOptionPriceCallAmericanBinomialPayout() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double y = 0.02;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double testVal = BinomialFormulas.optionPriceCallAmericanBinomialPayout(S, K, r, y, sigma, time, steps);
        assertEquals(13.5926, testVal, 0.0001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPriceCallAmericanBinomialPayout(S, K, r, y, sigma, time, steps);
        assertNotEquals(13.5926, testVal, 0.0001);
    }

    @Test
    public void testOptionPricePutAmericanBinomialPayout() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double y = 0.02;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double testVal = BinomialFormulas.optionPricePutAmericanBinomialPayout(S, K, r, y, sigma, time, steps);
        assertEquals(6.99407, testVal, 0.00001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPricePutAmericanBinomialPayout(S, K, r, y, sigma, time, steps);
        assertNotEquals(6.99407, testVal, 0.00001);
    }

    @Test
    public void testOptionPriceCallEuropeanBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double testVal = BinomialFormulas.optionPriceCallEuropeanBinomial(S, K, r, sigma, time, steps);
        assertEquals(14.9505, testVal, 0.0001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPriceCallEuropeanBinomial(S, K, r, sigma, time, steps);
        assertNotEquals(14.9505, testVal, 0.0001);
    }

    @Test
    public void testOptionPricePutEuropeanBinomial() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        int steps = 100;
        double testVal = BinomialFormulas.optionPricePutEuropeanBinomial(S, K, r, sigma, time, steps);
        assertEquals(5.43425, testVal, 0.00001);

        //Negative test
        sigma = 0.26;
        testVal = BinomialFormulas.optionPricePutEuropeanBinomial(S, K, r, sigma, time, steps);
        assertNotEquals(5.43425, testVal, 0.00001);
    }
}