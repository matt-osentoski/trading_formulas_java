package com.garagebandhedgefund.trading.formulas;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Matt on 4/30/2016.
 */
public class PriceChangeFormulasTest {
    @Test
    public void percentPriceChange() throws Exception {
        double first = PriceChangeFormulas.percentChange(117.34, 116.28, 2);
        assertEquals(-0.90, first, 0.02);
        double second = PriceChangeFormulas.percentChange(116.28, 117.34, 2);
        assertEquals(0.90, second, 0.02);
        double third = PriceChangeFormulas.percentChange(116.28, 116.28, 3);
        assertEquals(0.00, third, 0.02);

        // Negative tests
        double firstNeg = PriceChangeFormulas.percentChange(117.34, 116.28, 2);
        assertNotEquals(-0.93, firstNeg, 0.02);
        double secondNeg = PriceChangeFormulas.percentChange(116.28, 117.34, 2);
        assertNotEquals(0.93, secondNeg, 0.02);
        double thirdNeg = PriceChangeFormulas.percentChange(116.28, 116.28, 2);
        assertNotEquals(0.03, thirdNeg, 0.02);

        // Test volumes
        double firstVol = PriceChangeFormulas.percentChange(34701000, 33199000, 2);
        assertEquals(-4.33, firstVol, 0.02);
        double secondVol = PriceChangeFormulas.percentChange(33199000, 34701000, 2);
        assertEquals(4.52, secondVol, 0.02);
        double thirdVol = PriceChangeFormulas.percentChange(34701000, 34701000, 2);
        assertEquals(0.00, thirdVol, 0.02);

        // Negative tests
        double firstVolNeg = PriceChangeFormulas.percentChange(34701000, 33199000, 2);
        assertNotEquals(-4.36, firstVolNeg, 0.02);
        double secondVolNeg = PriceChangeFormulas.percentChange(33199000, 34701000, 2);
        assertNotEquals(4.36, secondVolNeg, 0.02);
        double thirdVolNeg = PriceChangeFormulas.percentChange(34701000, 34701000, 2);
        assertNotEquals(0.03, thirdVolNeg, 0.02);
    }

}