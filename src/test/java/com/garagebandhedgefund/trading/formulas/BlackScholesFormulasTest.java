package com.garagebandhedgefund.trading.formulas;

import com.garagebandhedgefund.trading.model.OptionPartials;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class BlackScholesFormulasTest {

    @Test
    public void testNormDist() throws Exception {
        double testVal = BlackScholesFormulas.normDist(1.23);
        assertEquals(0.187235, testVal, 0.000001);

        //Negative test
        testVal = BlackScholesFormulas.normDist(1.24);
        assertNotEquals(0.187235, testVal, 0.000001);
    }

    @Test
    public void testCumulativeNormDist() throws Exception {
        double testVal = BlackScholesFormulas.cumulativeNormDist(1.23);
        assertEquals(0.890651, testVal, 0.000001);

        //Negative test
        testVal = BlackScholesFormulas.cumulativeNormDist(1.24);
        assertNotEquals(0.890651, testVal, 0.000001);
    }

    @Test
    public void testOptionPriceCallBlackScholes() throws Exception {
        double S = 50;
        double K = 50;
        double r = 0.10;
        double sigma = 0.30;
        double time = 0.50;
        double testVal = BlackScholesFormulas.optionPriceCallBlackScholes(S, K, r, sigma, time);
        assertEquals(5.45325, testVal, 0.00001);

        //Negative test
        sigma = 0.31;
        testVal = BlackScholesFormulas.optionPriceCallBlackScholes(S, K, r, sigma, time);
        assertNotEquals(5.45325, testVal, 0.00001);
    }

    @Test
    public void testOptionPricePutBlackScholes() throws Exception {
        double S = 50;
        double K = 50;
        double r = 0.10;
        double sigma = 0.30;
        double time = 0.50;
        double testVal = BlackScholesFormulas.optionPricePutBlackScholes(S, K, r, sigma, time);
        assertEquals(3.01472, testVal, 0.00001);

        //Negative test
        sigma = 0.31;
        testVal = BlackScholesFormulas.optionPricePutBlackScholes(S, K, r, sigma, time);
        assertNotEquals(3.01472, testVal, 0.00001);
    }

    @Test
    public void testOptionPriceDeltaCallBlackScholes() throws Exception {
        double S = 50;
        double K = 50;
        double r = 0.10;
        double sigma = 0.30;
        double time = 0.50;
        double testVal = BlackScholesFormulas.optionPriceDeltaCallBlackScholes(S, K, r, sigma, time);
        assertEquals(0.633737, testVal, 0.000001);

        //Negative test
        sigma = 0.31;
        testVal = BlackScholesFormulas.optionPriceDeltaCallBlackScholes(S, K, r, sigma, time);
        assertNotEquals(0.633737, testVal, 0.000001);
    }

    @Test
    public void testOptionPriceDeltaPutBlackScholes() throws Exception {
        double S = 50;
        double K = 50;
        double r = 0.10;
        double sigma = 0.30;
        double time = 0.50;
        double testVal = BlackScholesFormulas.optionPriceDeltaPutBlackScholes(S, K, r, sigma, time);
        assertEquals(-0.366263, testVal, 0.000001);

        //Negative test
        sigma = 0.31;
        testVal = BlackScholesFormulas.optionPriceDeltaPutBlackScholes(S, K, r, sigma, time);
        assertNotEquals(-0.366263, testVal, 0.000001);
    }

    @Test
    public void testOptionPriceImpliedVolatilityCallBlackScholesBisections() throws Exception {
        double S = 50;
        double K = 50;
        double r = 0.10;
        double optionPrice = 2.5;
        double time = 0.50;
        double testVal = BlackScholesFormulas.optionPriceImpliedVolatilityCallBlackScholesBisections(S, K, r, time, optionPrice);
        assertEquals(0.0500419, testVal, 0.0000001);

        //Negative test
        optionPrice = 2.6;
        testVal = BlackScholesFormulas.optionPriceImpliedVolatilityCallBlackScholesBisections(S, K, r, time, optionPrice);
        assertNotEquals(0.0500419, testVal, 0.0000001);
    }

    @Test
    public void testOptionPriceImpliedVolatilityCallBlackScholesNewton() throws Exception {
        double S = 50;
        double K = 50;
        double r = 0.10;
        double optionPrice = 2.5;
        double time = 0.50;
        double testVal = BlackScholesFormulas.optionPriceImpliedVolatilityCallBlackScholesNewton(S, K, r, time, optionPrice);
        assertEquals(0.0500427, testVal, 0.0000001);

        //Negative test
        optionPrice = 2.6;
        testVal = BlackScholesFormulas.optionPriceImpliedVolatilityCallBlackScholesNewton(S, K, r, time, optionPrice);
        assertNotEquals(0.0500427, testVal, 0.0000001);
    }

    @Test
    public void testOptionPricePartialsCallBlackScholes() throws Exception {
        double S = 50;
        double K = 50;
        double r = 0.10;
        double sigma = 0.30;
        double time = 0.50;

        OptionPartials partial = BlackScholesFormulas.optionPricePartialsCallBlackScholes(S, K, r, sigma, time);
        assertEquals(0.633737, partial.getDelta(), 0.000001);
        assertEquals(0.0354789, partial.getGamma(), 0.0000001);
        assertEquals(-6.61473, partial.getTheta(), 0.00001);
        assertEquals(13.3046, partial.getVega(), 0.0001);
        assertEquals(13.1168, partial.getRho(), 0.0001);

        //Negative test
        sigma = 0.31;
        partial = BlackScholesFormulas.optionPricePartialsCallBlackScholes(S, K, r, sigma, time);
        assertNotEquals(0.633737, partial.getDelta(), 0.000001);
        assertNotEquals(0.0354789, partial.getGamma(), 0.0000001);
        assertNotEquals(-6.61473, partial.getTheta(), 0.00001);
        assertNotEquals(13.3046, partial.getVega(), 0.0001);
        assertNotEquals(13.1168, partial.getRho(), 0.0001);
    }

    @Test
    public void testOptionPricePartialsPutBlackScholes() throws Exception {
        double S = 50;
        double K = 50;
        double r = 0.10;
        double sigma = 0.30;
        double time = 0.50;

        OptionPartials partial = BlackScholesFormulas.optionPricePartialsPutBlackScholes(S, K, r, sigma, time);
        assertEquals(-0.366263, partial.getDelta(), 0.000001);
        assertEquals(0.0354789, partial.getGamma(), 0.0000001);
        assertEquals(-1.85859, partial.getTheta(), 0.00001);
        assertEquals(13.3046, partial.getVega(), 0.0001);
        assertEquals(-10.6639, partial.getRho(), 0.0001);

        //Negative test
        sigma = 0.31;
        partial = BlackScholesFormulas.optionPricePartialsPutBlackScholes(S, K, r, sigma, time);
        assertNotEquals(-0.366263, partial.getDelta(), 0.000001);
        assertNotEquals(0.0354789, partial.getGamma(), 0.0000001);
        assertNotEquals(-1.85859, partial.getTheta(), 0.00001);
        assertNotEquals(13.3046, partial.getVega(), 0.0001);
        assertNotEquals(-10.6639, partial.getRho(), 0.0001);
    }

    @Test
    public void testOptionPriceEuropeanCallPayout() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double q = 0.05;
        double sigma = 0.25;
        double time = 1.0;
        double testVal = BlackScholesFormulas.optionPriceEuropeanCallPayout(S, K, r, q, sigma, time);
        assertEquals(11.7344, testVal, 0.0001);

        //Negative test
        sigma = 0.26;
        testVal = BlackScholesFormulas.optionPriceEuropeanCallPayout(S, K, r, q, sigma, time);
        assertNotEquals(11.7344, testVal, 0.0001);
    }

    @Test
    public void testOptionPriceEuropeanPutPayout() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double q = 0.05;
        double sigma = 0.25;
        double time = 1.0;
        double testVal = BlackScholesFormulas.optionPriceEuropeanPutPayout(S, K, r, q, sigma, time);
        assertEquals(7.09515, testVal, 0.00001);

        //Negative test
        sigma = 0.26;
        testVal = BlackScholesFormulas.optionPriceEuropeanPutPayout(S, K, r, q, sigma, time);
        assertNotEquals(7.09515, testVal, 0.00001);
    }

    @Test
    public void testOptionPriceEuropeanCallDividends() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        double[] dividendTimes = new double[] { 0.25, 0.75 };
        double[] dividendAmounts = new double[] { 2.5, 2.5 };
        double testVal = BlackScholesFormulas.optionPriceEuropeanCallDividends(S, K, r, sigma,
                time, dividendTimes, dividendAmounts);
        assertEquals(11.8094, testVal, 0.0001);

        //Negative test
        sigma = 0.26;
        testVal = BlackScholesFormulas.optionPriceEuropeanCallDividends(S, K, r, sigma,
                time, dividendTimes, dividendAmounts);
        assertNotEquals(11.8094, testVal, 0.0001);
    }

    @Test
    public void testOptionPriceEuropeanPutDividends() throws Exception {
        double S = 100;
        double K = 100;
        double r = 0.10;
        double sigma = 0.25;
        double time = 1.0;
        double[] dividendTimes = new double[] { 0.25, 0.75 };
        double[] dividendAmounts = new double[] { 2.5, 2.5 };
        double testVal = BlackScholesFormulas.optionPriceEuropeanPutDividends(S, K, r, sigma,
                time, dividendTimes, dividendAmounts);
        assertEquals(7.05077, testVal, 0.00001);

        //Negative test
        sigma = 0.26;
        testVal = BlackScholesFormulas.optionPriceEuropeanPutDividends(S, K, r, sigma,
                time, dividendTimes, dividendAmounts);
        assertNotEquals(7.05077, testVal, 0.00001);
    }
}