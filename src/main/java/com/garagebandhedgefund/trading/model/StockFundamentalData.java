package com.garagebandhedgefund.trading.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 1/2/2016
 * Time: 11:12 PM
 */
@Entity
@Table(name="stock_fundamental_data")
public class StockFundamentalData implements Serializable {

    private static final long serialVersionUID = 1468043752618247864L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "stock_id", nullable = false)
    private Stock stock;

    private String symbol;

    @Column(name = "quarter_end_date")
    private Date quarterEndDate;

    @Column(name = "shares")
    private long shares;

    @Column(name = "shares_split_adj")
    private long sharesSplitAdjusted;

    @Column(name = "split_factor")
    private int splitFactor;

    @Column(name = "assets")
    private long assets;

    @Column(name = "liabilities")
    private long liabilities;

    @Column(name = "shareholder_equity")
    private long shareholderEquity;

    @Column(name = "non_controlling_interest")
    private long nonControllingInterest;

    @Column(name = "preferred_equity")
    private long preferredEquity;

    @Column(name = "goodwill_and_tangibles")
    private long goodwillAndTangibles;

    @Column(name = "long_term_debt")
    private long longTermDebt;

    @Column(name = "revenue")
    private long revenue;

    @Column(name = "earnings")
    private long earnings;

    @Column(name = "earnings_avail_for_comm_stkholders")
    private long earningsAvailForCommonStockholders;

    @Column(name = "eps_basic")
    private double epsBasic;

    @Column(name = "eps_diluted")
    private double epsDiluted;

    @Column(name = "dividend_per_share")
    private double dividendPerShare;

    @Column(name = "price")
    private double price;

    @Column(name = "price_high")
    private double priceHigh;

    @Column(name = "price_low")
    private double priceLow;

    @Column(name = "roe")
    private double roe;

    @Column(name = "roa")
    private double roa;

    @Column(name = "book_val_of_equity_per_share")
    private double bookValueOfEquityPerShare;

    @Column(name = "p_b_ratio")
    private double pbRatio;

    @Column(name = "p_e_ratio")
    private double peRatio;

    @Column(name = "cumulative_dividends")
    private double cumulativeDividends;

    @Column(name = "dividend_payout_ratio")
    private double dividendPayoutRatio;

    @Column(name = "long_term_debt_to_eqty_ratio")
    private double longTermDebtToEquityRatio;

    @Column(name = "equity_to_assets_ratio")
    private double equityToAssetsRatio;

    @Column(name = "net_margin")
    private double netMargin;

    @Column(name = "asset_turnover")
    private double assetTurnover;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Date getQuarterEndDate() {
        return quarterEndDate;
    }

    public void setQuarterEndDate(Date quarterEndDate) {
        this.quarterEndDate = quarterEndDate;
    }

    public long getShares() {
        return shares;
    }

    public void setShares(long shares) {
        this.shares = shares;
    }

    public long getSharesSplitAdjusted() {
        return sharesSplitAdjusted;
    }

    public void setSharesSplitAdjusted(long sharesSplitAdjusted) {
        this.sharesSplitAdjusted = sharesSplitAdjusted;
    }

    public int getSplitFactor() {
        return splitFactor;
    }

    public void setSplitFactor(int splitFactor) {
        this.splitFactor = splitFactor;
    }

    public long getAssets() {
        return assets;
    }

    public void setAssets(long assets) {
        this.assets = assets;
    }

    public long getLiabilities() {
        return liabilities;
    }

    public void setLiabilities(long liabilities) {
        this.liabilities = liabilities;
    }

    public long getShareholderEquity() {
        return shareholderEquity;
    }

    public void setShareholderEquity(long shareholderEquity) {
        this.shareholderEquity = shareholderEquity;
    }

    public long getNonControllingInterest() {
        return nonControllingInterest;
    }

    public void setNonControllingInterest(long nonControllingInterest) {
        this.nonControllingInterest = nonControllingInterest;
    }

    public long getPreferredEquity() {
        return preferredEquity;
    }

    public void setPreferredEquity(long preferredEquity) {
        this.preferredEquity = preferredEquity;
    }

    public long getGoodwillAndTangibles() {
        return goodwillAndTangibles;
    }

    public void setGoodwillAndTangibles(long goodwillAndTangibles) {
        this.goodwillAndTangibles = goodwillAndTangibles;
    }

    public long getLongTermDebt() {
        return longTermDebt;
    }

    public void setLongTermDebt(long longTermDebt) {
        this.longTermDebt = longTermDebt;
    }

    public long getRevenue() {
        return revenue;
    }

    public void setRevenue(long revenue) {
        this.revenue = revenue;
    }

    public long getEarnings() {
        return earnings;
    }

    public void setEarnings(long earnings) {
        this.earnings = earnings;
    }

    public long getEarningsAvailForCommonStockholders() {
        return earningsAvailForCommonStockholders;
    }

    public void setEarningsAvailForCommonStockholders(long earningsAvailForCommonStockholders) {
        this.earningsAvailForCommonStockholders = earningsAvailForCommonStockholders;
    }

    public double getEpsBasic() {
        return epsBasic;
    }

    public void setEpsBasic(double epsBasic) {
        this.epsBasic = epsBasic;
    }

    public double getEpsDiluted() {
        return epsDiluted;
    }

    public void setEpsDiluted(double epsDiluted) {
        this.epsDiluted = epsDiluted;
    }

    public double getDividendPerShare() {
        return dividendPerShare;
    }

    public void setDividendPerShare(double dividendPerShare) {
        this.dividendPerShare = dividendPerShare;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPriceHigh() {
        return priceHigh;
    }

    public void setPriceHigh(double priceHigh) {
        this.priceHigh = priceHigh;
    }

    public double getPriceLow() {
        return priceLow;
    }

    public void setPriceLow(double priceLow) {
        this.priceLow = priceLow;
    }

    public double getRoe() {
        return roe;
    }

    public void setRoe(double roe) {
        this.roe = roe;
    }

    public double getRoa() {
        return roa;
    }

    public void setRoa(double roa) {
        this.roa = roa;
    }

    public double getBookValueOfEquityPerShare() {
        return bookValueOfEquityPerShare;
    }

    public void setBookValueOfEquityPerShare(double bookValueOfEquityPerShare) {
        this.bookValueOfEquityPerShare = bookValueOfEquityPerShare;
    }

    public double getPbRatio() {
        return pbRatio;
    }

    public void setPbRatio(double pbRatio) {
        this.pbRatio = pbRatio;
    }

    public double getPeRatio() {
        return peRatio;
    }

    public void setPeRatio(double peRatio) {
        this.peRatio = peRatio;
    }

    public double getCumulativeDividends() {
        return cumulativeDividends;
    }

    public void setCumulativeDividends(double cumulativeDividends) {
        this.cumulativeDividends = cumulativeDividends;
    }

    public double getDividendPayoutRatio() {
        return dividendPayoutRatio;
    }

    public void setDividendPayoutRatio(double dividendPayoutRatio) {
        this.dividendPayoutRatio = dividendPayoutRatio;
    }

    public double getLongTermDebtToEquityRatio() {
        return longTermDebtToEquityRatio;
    }

    public void setLongTermDebtToEquityRatio(double longTermDebtToEquityRatio) {
        this.longTermDebtToEquityRatio = longTermDebtToEquityRatio;
    }

    public double getEquityToAssetsRatio() {
        return equityToAssetsRatio;
    }

    public void setEquityToAssetsRatio(double equityToAssetsRatio) {
        this.equityToAssetsRatio = equityToAssetsRatio;
    }

    public double getNetMargin() {
        return netMargin;
    }

    public void setNetMargin(double netMargin) {
        this.netMargin = netMargin;
    }

    public double getAssetTurnover() {
        return assetTurnover;
    }

    public void setAssetTurnover(double assetTurnover) {
        this.assetTurnover = assetTurnover;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
}
