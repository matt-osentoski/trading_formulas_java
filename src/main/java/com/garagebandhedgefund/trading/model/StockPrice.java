package com.garagebandhedgefund.trading.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 8/22/14
 * Time: 2:12 PM
 */
@Entity
@Table(name="stock_prices")
public class StockPrice implements Serializable {

    private static final long serialVersionUID = -6054934172560436677L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "stock_id", nullable = true)
    private Stock stock;

    private String symbol;
    private int volume;

    @Column(name = "opening_price")
    private double openingPrice;

    @Column(name = "closing_price")
    private double closingPrice;

    @Column(name = "adj_closing_price")
    private double adjClosingPrice;

    @Column(name = "low_price")
    private double lowPrice;

    @Column(name = "high_price")
    private double highPrice;

    @Column(name = "price_date")
    private Date priceDate;

    private String period;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public double getOpeningPrice() {
        return openingPrice;
    }

    public void setOpeningPrice(double openingPrice) {
        this.openingPrice = openingPrice;
    }

    public double getClosingPrice() {
        return closingPrice;
    }

    public void setClosingPrice(double closingPrice) {
        this.closingPrice = closingPrice;
    }

    public double getAdjClosingPrice() {
        return adjClosingPrice;
    }

    public void setAdjClosingPrice(double adjClosingPrice) {
        this.adjClosingPrice = adjClosingPrice;
    }

    public double getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(double lowPrice) {
        this.lowPrice = lowPrice;
    }

    public double getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(double highPrice) {
        this.highPrice = highPrice;
    }

    public Date getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(Date priceDate) {
        this.priceDate = priceDate;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
}
