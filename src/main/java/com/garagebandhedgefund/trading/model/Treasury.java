package com.garagebandhedgefund.trading.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Matt Osentoski on 9/8/2014.
 */
@Entity
@Table(name="treasuries")
public class Treasury implements Serializable {

    private static final long serialVersionUID = 2745517908131552761L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private String id;

    @Column(name = "time_period")
    private Date timePeriod;

    @Column(name = "1_month")
    private double c1month;

    @Column(name = "3_month")
    private double c3month;

    @Column(name = "6_month")
    private double c6month;

    @Column(name = "1_year")
    private double c1year;

    @Column(name = "2_year")
    private double c2year;

    @Column(name = "3_year")
    private double c3year;

    @Column(name = "5_year")
    private double c5year;

    @Column(name = "7_year")
    private double c7year;

    @Column(name = "10_year")
    private double c10year;

    @Column(name = "20_year")
    private double c20year;

    @Column(name = "30_year")
    private double c30year;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(Date timePeriod) {
        this.timePeriod = timePeriod;
    }

    public double getC1month() {
        return c1month;
    }

    public void setC1month(double c1month) {
        this.c1month = c1month;
    }

    public double getC3month() {
        return c3month;
    }

    public void setC3month(double c3month) {
        this.c3month = c3month;
    }

    public double getC6month() {
        return c6month;
    }

    public void setC6month(double c6month) {
        this.c6month = c6month;
    }

    public double getC1year() {
        return c1year;
    }

    public void setC1year(double c1year) {
        this.c1year = c1year;
    }

    public double getC2year() {
        return c2year;
    }

    public void setC2year(double c2year) {
        this.c2year = c2year;
    }

    public double getC3year() {
        return c3year;
    }

    public void setC3year(double c3year) {
        this.c3year = c3year;
    }

    public double getC5year() {
        return c5year;
    }

    public void setC5year(double c5year) {
        this.c5year = c5year;
    }

    public double getC7year() {
        return c7year;
    }

    public void setC7year(double c7year) {
        this.c7year = c7year;
    }

    public double getC10year() {
        return c10year;
    }

    public void setC10year(double c10year) {
        this.c10year = c10year;
    }

    public double getC20year() {
        return c20year;
    }

    public void setC20year(double c20year) {
        this.c20year = c20year;
    }

    public double getC30year() {
        return c30year;
    }

    public void setC30year(double c30year) {
        this.c30year = c30year;
    }
}
