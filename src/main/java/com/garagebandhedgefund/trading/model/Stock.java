package com.garagebandhedgefund.trading.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 8/22/14
 * Time: 2:10 PM
 */
@Entity
@Table(name="stocks")
public class Stock implements Serializable {

    private static final long serialVersionUID = 7668126525083457139L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    private String symbol;
    private String name;
    private String description;

    @Column(name = "market_cap")
    private double marketCap;

    @Column(name = "ipo_year")
    private String ipoYear;
    private String sector;
    private String industry;

    @Column(name = "stock_index")
    private String stockIndex;

    @Column(name = "underlying_symbol")
    private String underlyingSymbol;

    @Column(name = "is_index")
    private int isIndex;

    @Column(name = "is_etf")
    private int isEtf;

    @Column(name = "is_short_etf")
    private int isShortEtf;

    @Column(name = "etf_leverage_multiplier")
    private double etfLeverageMultiplier = 1.0;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="stock_id", referencedColumnName="id")
    private List<StockPrice> prices;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(double marketCap) {
        this.marketCap = marketCap;
    }

    public String getIpoYear() {
        return ipoYear;
    }

    public void setIpoYear(String ipoYear) {
        this.ipoYear = ipoYear;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getStockIndex() {
        return stockIndex;
    }

    public void setStockIndex(String stockIndex) {
        this.stockIndex = stockIndex;
    }

    public List<StockPrice> getPrices() {
        return prices;
    }

    public void setPrices(List<StockPrice> prices) {
        this.prices = prices;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUnderlyingSymbol() {
        return underlyingSymbol;
    }

    public void setUnderlyingSymbol(String underlyingSymbol) {
        this.underlyingSymbol = underlyingSymbol;
    }

    public int getIsEtf() {
        return isEtf;
    }

    public void setIsEtf(int isEtf) {
        this.isEtf = isEtf;
    }

    public int getIsShortEtf() {
        return isShortEtf;
    }

    public void setIsShortEtf(int isShortEtf) {
        this.isShortEtf = isShortEtf;
    }

    public double getEtfLeverageMultiplier() {
        return etfLeverageMultiplier;
    }

    public void setEtfLeverageMultiplier(double etfLeverageMultiplier) {
        this.etfLeverageMultiplier = etfLeverageMultiplier;
    }

    public int getIsIndex() {
        return isIndex;
    }

    public void setIsIndex(int isIndex) {
        this.isIndex = isIndex;
    }
}

