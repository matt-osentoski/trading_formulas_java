package com.garagebandhedgefund.trading.model;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 8/22/14
 * Time: 2:09 PM
 */
public class PivotPoint {
    private double p;
    private double r1;
    private double r2;
    private double r3;
    private double s1;
    private double s2;
    private double s3;

    public double getP() {
        return p;
    }

    public void setP(double p) {
        this.p = p;
    }

    public double getR1() {
        return r1;
    }

    public void setR1(double r1) {
        this.r1 = r1;
    }

    public double getR2() {
        return r2;
    }

    public void setR2(double r2) {
        this.r2 = r2;
    }

    public double getR3() {
        return r3;
    }

    public void setR3(double r3) {
        this.r3 = r3;
    }

    public double getS1() {
        return s1;
    }

    public void setS1(double s1) {
        this.s1 = s1;
    }

    public double getS2() {
        return s2;
    }

    public void setS2(double s2) {
        this.s2 = s2;
    }

    public double getS3() {
        return s3;
    }

    public void setS3(double s3) {
        this.s3 = s3;
    }
}
