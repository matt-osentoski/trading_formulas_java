package com.garagebandhedgefund.trading.exceptions;

public class StopLossException extends RuntimeException {
    public StopLossException() {super();}
    public StopLossException(String message) {super(message);}
    public StopLossException(String message, Throwable cause) {
        super(message, cause);
    }
    public StopLossException(Throwable cause) {super(cause);}
}
