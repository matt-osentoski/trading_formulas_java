package com.garagebandhedgefund.trading.vo;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 *
 * This class stores Stochastic values
 *
 * Date: 8/22/14
 * Time: 2:27 PM
 */
public class StochasticVO {
    private double k;
    private double d;
    private double kSlow;
    private double dSlow;

    public double getK() {
        return k;
    }

    public void setK(double k) {
        this.k = k;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public double getkSlow() {
        return kSlow;
    }

    public void setkSlow(double kSlow) {
        this.kSlow = kSlow;
    }

    public double getdSlow() {
        return dSlow;
    }

    public void setdSlow(double dSlow) {
        this.dSlow = dSlow;
    }
}
