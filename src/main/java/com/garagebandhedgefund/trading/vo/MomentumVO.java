package com.garagebandhedgefund.trading.vo;

import com.garagebandhedgefund.trading.model.StockPrice;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * This class is used to hold MACD / PPO EMA values for calculating the momentum indicators EMA
 * Date: 8/26/14
 * Time: 3:26 PM
 */
public class MomentumVO extends StockPrice {
    private double ema_12;
    private double ema_26;
    private double macd_12_26;
    private double ppo_12_26;

    public double getEma_12() {
        return ema_12;
    }

    public void setEma_12(double ema_12) {
        this.ema_12 = ema_12;
    }

    public double getEma_26() {
        return ema_26;
    }

    public void setEma_26(double ema_26) {
        this.ema_26 = ema_26;
    }

    public double getMacd_12_26() {
        return macd_12_26;
    }

    public void setMacd_12_26(double macd_12_26) {
        this.macd_12_26 = macd_12_26;
    }

    public double getPpo_12_26() {
        return ppo_12_26;
    }

    public void setPpo_12_26(double ppo_12_26) {
        this.ppo_12_26 = ppo_12_26;
    }
}
