package com.garagebandhedgefund.trading.vo;

public class StopLossVO {
    private double stopLoss;
    private int shares;

    public double getStopLoss() {
        return stopLoss;
    }

    public void setStopLoss(double stopLoss) {
        this.stopLoss = stopLoss;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }
}
