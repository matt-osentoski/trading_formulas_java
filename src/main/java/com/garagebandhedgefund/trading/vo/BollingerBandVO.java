package com.garagebandhedgefund.trading.vo;

/**
 * This class stores Bollinger Band values
 */
public class BollingerBandVO {
    private double upperBand;
    private double lowerBand;
    private double bandwidth;

    public double getUpperBand() {
        return upperBand;
    }

    public void setUpperBand(double upperBand) {
        this.upperBand = upperBand;
    }

    public double getLowerBand() {
        return lowerBand;
    }

    public void setLowerBand(double lowerBand) {
        this.lowerBand = lowerBand;
    }

    public double getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(double bandwidth) {
        this.bandwidth = bandwidth;
    }
}
