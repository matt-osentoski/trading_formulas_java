package com.garagebandhedgefund.trading.formulas;

import com.garagebandhedgefund.trading.exceptions.StopLossException;
import com.garagebandhedgefund.trading.model.StockPrice;
import com.garagebandhedgefund.trading.vo.StopLossVO;
import org.apache.commons.math3.stat.descriptive.moment.Mean;

import java.util.List;

/**
 * Created by Matt Osentoski
 *
 * This class contains methods used to calculate stop losses and manage risks with trades.
 */
public class MoneyManagementFormulas {

    /**
     * (NOTE: This method is for long positions only)
     * This method is used to determine the minimum price you should set as a stop loss for a particular trade and
     * the number of shares that would meet your risk tolerance.
     *
     * The methodology used is to not risk more than a certain percentage of total account balance on any given trade.
     * For example, 2% should be the most risked on any given trade.
     * <br><br>
     * In addition, if the price that is returned is less than the average amount the stock has been moving, the method
     * will throw a StopLossException stating that this trade is incurring a large amount of risk of stopping out prematurely.
     * The number of days used for the average is adjustable and defaults to 20.
     *
     * @param stockPrices A list of prices for the security you are interested in trading
     * @param accountBalance The total balance of your trading account at the time of this trade
     * @param currentSharePrice The current price of the security you're interested in trading
     * @param maxAtRiskPerTrade The maximum amount of your total account balance you want to risk per trade. Defaults to 2% (0.02)
     * @param priceRangeDays The number of days to go back in the 'prices' argument to determine the average range the security moves. Defaults to 20
     * @return Money Management object containing the stop price and maximum number of shares that can be purchased
     */
    public static StopLossVO createStopLoss(List<StockPrice> stockPrices, double accountBalance,
                                            double currentSharePrice, double maxAtRiskPerTrade,
                                            int priceRangeDays) {
        StopLossVO mm = new StopLossVO();
        if (priceRangeDays <= 0) priceRangeDays = 20;
        if (maxAtRiskPerTrade <= 0) maxAtRiskPerTrade = 0.02; // 2% at risk

        double averagePriceRange = getAveragePriceRange(stockPrices, priceRangeDays);
        double maxRiskBalance = accountBalance * maxAtRiskPerTrade;
        int shares = (int) Math.round(maxRiskBalance / averagePriceRange);
        if (shares < 1) {
            throw new StopLossException("This trade will exceed your risk tolerance parameters. This trade should be " +
                    "avoided");
        }
        double stopLoss = currentSharePrice - averagePriceRange;

        mm.setShares(shares);
        mm.setStopLoss(stopLoss);
        return mm;
    }

    /**
     * (NOTE: This method is for long positions only)
     * This method is used to determine the minimum price you should set as a stop loss for a particular trade and
     * the number of shares that would meet your risk tolerance.
     *
     * The methodology used is to not risk more than a certain percentage of total account balance on any given trade.
     * For example, 2% should be the most risked on any given trade.
     * <br><br>
     * In addition, if the price that is returned is less than the average amount the stock has been moving, the method
     * will throw a StopLossException stating that this trade is incurring a large amount of risk of stopping out prematurely.
     * The number of days used for the average is adjustable and defaults to 20.
     *
     * @param stockPrices A list of prices for the security you are interested in trading
     * @param accountBalance The total balance of your trading account at the time of this trade
     * @param currentSharePrice The current price of the security you're interested in trading
     * @return Money Management object containing the stop price and maximum number of shares that can be purchased
     */
    public static StopLossVO createStopLoss(List<StockPrice> stockPrices, double accountBalance,
                                            double currentSharePrice) {
        return MoneyManagementFormulas.createStopLoss(stockPrices, accountBalance, currentSharePrice, 0, 0);
    }

    /**
     * Determine the average price range for this security in the last N, number of days.
     * @param stockPrices A list of prices for the security you are interested in trading
     * @param priceRangeDays The number of days to go back in the 'prices' argument to determine the average range the security moves.  The stop loss should not be less than this range.
     * @return Average price range
     */
    public static double getAveragePriceRange(List<StockPrice> stockPrices, int priceRangeDays) {
        List<StockPrice> subList = stockPrices;
        if (priceRangeDays < stockPrices.size()) {
            subList = stockPrices.subList(stockPrices.size()-priceRangeDays, stockPrices.size());
        }
        Mean mean = new Mean();
        for(StockPrice price: subList) {
            if (price != null) {
                mean.increment(price.getHighPrice()-price.getLowPrice());
            }
        }
        return mean.getResult();
    }

    /**
     * Determines the maximum number of shares that can be purchased based on the account balance
     * @param accountBalance The total balance of your trading account at the time of this trade
     * @param currentSharePrice The current price of the security you're interested in trading
     * @return Maximum number of shares
     */
    public static int maxSharesByAccountBalance(double accountBalance, double currentSharePrice) {
        int shares = (int) Math.round(accountBalance / currentSharePrice);
        return shares;
    }

}
