package com.garagebandhedgefund.trading.formulas;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Matt on 4/30/2016.
 */
public class PriceChangeFormulas {

    /**
     *
     * @param originalValue
     * @param newValue
     * @param precision
     * @return
     */
    public static double percentChange(double originalValue, double newValue, int precision) {
        double percentChange = 0;
        if (originalValue < newValue) {
            double increase = newValue - originalValue;
            percentChange = (increase/originalValue) * 100;
        } else if ( originalValue > newValue) {
            double decrease = originalValue - newValue;
            percentChange = (decrease/originalValue) * -100;
        }
        BigDecimal bd = new BigDecimal(percentChange);
        bd = bd.setScale(precision, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
