package com.garagebandhedgefund.trading.formulas;

import com.garagebandhedgefund.trading.model.OptionPartials;

/**
 * Created by Matt Osentoski on 8/15/2014.
 *
 * This class contains formulas based on binomial equations
 *
 * Converted to C# from "Financial Numerical Recipes in C" by:
 * Bernt Arne Odegaard
 * http://finance.bi.no/~bernt/gcc_prog/index.html
 */
public class BinomialFormulas {
    /**
     * American Option (Call) using binomial approximations
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param t time to maturity 
     * @param steps Number of steps in binomial tree 
     * @return Option price
     */
    public static double optionPriceCallAmericanBinomial(double S, double K, double r,
                                                         double sigma, double t, int steps) {
        double R = Math.exp(r*(t/steps));
        double Rinv = 1.0/R;
        double u = Math.exp(sigma*Math.sqrt(t/steps));
        double d = 1.0/u;
        double p_up = (R-d)/(u-d);
        double p_down = 1.0-p_up;

        double[] prices = new double[steps+1];       // price of underlying
        prices[0] = S*Math.pow(d, steps);  // fill in the endnodes.
        double uu = u*u;
        for (int i=1; i<=steps; ++i)
            prices[i] = uu*prices[i-1];

        double[] call_values = new double[steps+1];       // value of corresponding call
        for (int i=0; i<=steps; ++i)
            call_values[i] = Math.max(0.0, (prices[i]-K)); // call payoffs at maturity

        for (int step=steps-1; step>=0; --step) {
            for (int i=0; i<=step; ++i) {
                call_values[i] = (p_up*call_values[i+1]+p_down*call_values[i])*Rinv;
                prices[i] = d*prices[i+1];
                call_values[i] = Math.max(call_values[i],prices[i]-K);       // check for exercise
            }
        }
        return call_values[0];
    }

    /**
     * American Option (Put) using binomial approximations
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param t time to maturity 
     * @param steps Number of steps in binomial tree 
     * @return Option price
     */
    public static double optionPricePutAmericanBinomial(double S, double K, double r,
                                                        double sigma, double t, int steps) {
        double R = Math.exp(r*(t/steps));            // interest rate for each step
        double Rinv = 1.0/R;                    // inverse of interest rate
        double u = Math.exp(sigma*Math.sqrt(t/steps));    // up movement
        double uu = u*u;
        double d = 1.0/u;
        double p_up = (R-d)/(u-d);
        double p_down = 1.0-p_up;
        double[] prices = new double[steps+1];       // price of underlying
        prices[0] = S*Math.pow(d, steps);
        for (int i=1; i<=steps; ++i) prices[i] = uu*prices[i-1];

        double[] put_values = new double[steps+1];       // value of corresponding put
        for (int i=0; i<=steps; ++i)
            put_values[i] = Math.max(0.0, (K-prices[i])); // put payoffs at maturity

        for (int step=steps-1; step>=0; --step) {
            for (int i=0; i<=step; ++i) {
                put_values[i] = (p_up*put_values[i+1]+p_down*put_values[i])*Rinv;
                prices[i] = d*prices[i+1];
                put_values[i] = Math.max(put_values[i],(K-prices[i]));    // check for exercise
            }
        }
        return put_values[0];
    }

    /**
     * Delta of an American Option (Call) using binomial approximations
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param t time to maturity 
     * @param noSteps Number of steps in binomial tree 
     * @return Delta of the option
     */
    public static double optionPriceDeltaAmericanCallBinomial(double S, double K, double r,
                                                              double sigma, double t, int noSteps) {
        double R = Math.exp(r*(t/noSteps));
        double Rinv = 1.0/R;
        double u = Math.exp(sigma*Math.sqrt(t/noSteps));
        double d = 1.0/u;
        double uu= u*u;
        double pUp   = (R-d)/(u-d);
        double pDown = 1.0 - pUp;

        double[] prices = new double[noSteps+1];
        prices[0] = S*Math.pow(d, noSteps);
        for (int i=1; i<=noSteps; ++i) prices[i] = uu*prices[i-1];

        double[] call_values = new double[noSteps+1];
        for (int i=0; i<=noSteps; ++i)
            call_values[i] = Math.max(0.0, (prices[i]-K));

        for (int CurrStep=noSteps-1 ; CurrStep>=1; --CurrStep) {
            for (int i=0; i<=CurrStep; ++i) {
                prices[i] = d*prices[i+1];
                call_values[i] = (pDown*call_values[i]+pUp*call_values[i+1])*Rinv;
                call_values[i] = Math.max(call_values[i], prices[i]-K);        // check for exercise
            }
        }
        double delta = (call_values[1]-call_values[0])/(S*u-S*d);
        return delta;
    }

    /**
     * Delta of an American Option (Put) using binomial approximations
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param t time to maturity 
     * @param noSteps Number of steps in binomial tree 
     * @return Delta of the option
     */
    public static double optionPriceDeltaAmericanPutBinomial(double S, double K, double r,
                                                             double sigma, double t, int noSteps) {
        double[] prices = new double[noSteps+1];
        double[] put_values = new double[noSteps+1];
        double R = Math.exp(r*(t/noSteps));
        double Rinv = 1.0/R;
        double u = Math.exp(sigma*Math.sqrt(t/noSteps));
        double d = 1.0/u;
        double uu= u*u;
        double pUp   = (R-d)/(u-d);
        double pDown = 1.0 - pUp;
        prices[0] = S*Math.pow(d, noSteps);
        int i;
        for (i=1; i<=noSteps; ++i)
            prices[i] = uu*prices[i-1];
        for (i=0; i<=noSteps; ++i)
            put_values[i] = Math.max(0.0, (K - prices[i]));
        for (int CurrStep=noSteps-1 ; CurrStep>=1; --CurrStep) {
            for (i=0; i<=CurrStep; ++i) {
                prices[i] = d*prices[i+1];
                put_values[i] = (pDown*put_values[i]+pUp*put_values[i+1])*Rinv;
                put_values[i] = Math.max(put_values[i], K-prices[i]);        // check for exercise
            };
        };
        double delta = (put_values[1]-put_values[0])/(S*u-S*d);
        return delta;
    }

    /**
     * Calculate partial derivatives for an American Option (Call) using
     * binomial approximations.
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param time time to maturity 
     * @param noSteps Number of steps in binomial tree 
     * @return OptionPartials Partial derivatives for this option
     */
    public static OptionPartials optionPricePartialsAmericanCallBinomial(double S, double K, double r,
                                                               double sigma, double time, int noSteps) {
        double[] prices = new double[noSteps+1];
        double[] call_values = new double[noSteps+1];
        double delta_t =(time/noSteps);
        double R = Math.exp(r*delta_t);
        double Rinv = 1.0/R;
        double u = Math.exp(sigma*Math.sqrt(delta_t));
        double d = 1.0/u;
        double uu= u*u;
        double pUp   = (R-d)/(u-d);
        double pDown = 1.0 - pUp;
        prices[0] = S*Math.pow(d, noSteps);
        for (int i=1; i<=noSteps; ++i)
            prices[i] = uu*prices[i-1];
        for (int i=0; i<=noSteps; ++i)
            call_values[i] = Math.max(0.0, (prices[i]-K));
        for (int CurrStep=noSteps-1; CurrStep>=2; --CurrStep) {
            for (int i=0; i<=CurrStep; ++i) {
                prices[i] = d*prices[i+1];
                call_values[i] = (pDown*call_values[i]+pUp*call_values[i+1])*Rinv;
                call_values[i] = Math.max(call_values[i], prices[i]-K);        // check for exercise
            }
        }
        double f22 = call_values[2];
        double f21 = call_values[1];
        double f20 = call_values[0];
        for (int i=0;i<=1;i++) {
            prices[i] = d*prices[i+1];
            call_values[i] = (pDown*call_values[i]+pUp*call_values[i+1])*Rinv;
            call_values[i] = Math.max(call_values[i], prices[i]-K);        // check for exercise
        }
        double f11 = call_values[1];
        double f10 = call_values[0];
        prices[0] = d*prices[1];
        call_values[0] = (pDown*call_values[0]+pUp*call_values[1])*Rinv;
        call_values[0] = Math.max(call_values[0], S-K);        // check for exercise on first date
        double f00 = call_values[0];
        double delta = (f11-f10)/(S*u-S*d);
        double h = 0.5 * S * ( uu - d*d);
        double gamma = ( (f22-f21)/(S*(uu-1)) - (f21-f20)/(S*(1-d*d)) ) / h;
        double theta = (f21-f00) / (2*delta_t);
        double diff = 0.02;
        double tmp_sigma = sigma+diff;
        double tmp_prices = optionPriceCallAmericanBinomial(S,K,r,tmp_sigma,time,noSteps);
        double vega = (tmp_prices-f00)/diff;
        diff = 0.05;
        double tmp_r = r+diff;
        tmp_prices = optionPriceCallAmericanBinomial(S, K, tmp_r, sigma, time, noSteps);
        double rho = (tmp_prices-f00)/diff;
        OptionPartials partials = new OptionPartials(delta,gamma,theta,vega,rho);
        return partials;
    }

    /**
     * Calculate partial derivatives for an American Option (Put) using
     * binomial approximations.
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param time time to maturity 
     * @param noSteps Number of steps in binomial tree 
     * @return OptionPartials Partial derivatives for this option
     */
    public static OptionPartials optionPricePartialsAmericanPutBinomial(double S, double K, double r,
                                                              double sigma, double time, int noSteps) {
        double[] prices = new double[noSteps+1];
        double[] put_values = new double[noSteps+1];
        double delta_t =(time/noSteps);
        double R = Math.exp(r*delta_t);
        double Rinv = 1.0/R;
        double u = Math.exp(sigma*Math.sqrt(delta_t));
        double d = 1.0/u;
        double uu= u*u;
        double pUp   = (R-d)/(u-d);
        double pDown = 1.0 - pUp;
        prices[0] = S*Math.pow(d, noSteps);
        int i;
        for (i=1; i<=noSteps; ++i)
            prices[i] = uu*prices[i-1];
        for (i=0; i<=noSteps; ++i)
            put_values[i] = Math.max(0.0, (K-prices[i]));
        for (int CurrStep=noSteps-1 ; CurrStep>=2; --CurrStep) {
            for (i=0; i<=CurrStep; ++i) {
                prices[i] = d*prices[i+1];
                put_values[i] = (pDown*put_values[i]+pUp*put_values[i+1])*Rinv;
                put_values[i] = Math.max(put_values[i], K-prices[i]); // check for exercise
            };
        };
        double f22 = put_values[2];
        double f21 = put_values[1];
        double f20 = put_values[0];
        for (i=0;i<=1;i++) {
            prices[i] = d*prices[i+1];
            put_values[i] = (pDown*put_values[i]+pUp*put_values[i+1])*Rinv;
            put_values[i] = Math.max(put_values[i], K-prices[i]); // check for exercise
        };
        double f11 = put_values[1];
        double f10 = put_values[0];
        prices[0] = d*prices[1];
        put_values[0] = (pDown*put_values[0]+pUp*put_values[1])*Rinv;
        put_values[0] = Math.max(put_values[0], K-prices[i]); // check for exercise
        double f00 = put_values[0];
        double delta = (f11-f10)/(S*(u-d));
        double h = 0.5 * S *( uu - d*d);
        double gamma = ( (f22-f21)/(S*(uu-1.0)) - (f21-f20)/(S*(1.0-d*d)) ) / h;
        double theta = (f21-f00) / (2*delta_t);
        double diff = 0.02;
        double tmp_sigma = sigma+diff;
        double tmp_prices = optionPricePutAmericanBinomial(S,K,r,tmp_sigma,time,noSteps);
        double vega = (tmp_prices-f00)/diff;
        diff = 0.05;
        double tmp_r = r+diff;
        tmp_prices = optionPricePutAmericanBinomial(S, K, tmp_r, sigma, time, noSteps);
        double rho = (tmp_prices-f00)/diff;
        OptionPartials partials = new OptionPartials(delta,gamma,theta,vega,rho);
        return partials;
    }

    /**
     * American Option (Call) for dividends with specific (discrete) dollar amounts
     * using binomial approximations
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param t time to maturity 
     * @param steps Number of steps in binomial tree 
     * @param dividendTimes Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year) 
     * @param dividendAmounts Array of dividend amounts for the 'dividendTimes' 
     * @return Option price
     */
    public static double optionPriceCallAmericanDiscreteDividendsBinomial(double S, double K,
            double r, double sigma, double t, int steps, double[] dividendTimes, double[] dividendAmounts) {
        int no_dividends = dividendTimes.length;
        if (no_dividends==0)
            return optionPriceCallAmericanBinomial(S,K,r,sigma,t,steps);// just do regular
        int steps_before_dividend = (int)(dividendTimes[0]/t*steps);
        double R = Math.exp(r*(t/steps));
        double Rinv = 1.0/R;
        double u = Math.exp(sigma*Math.sqrt(t/steps));
        double d = 1.0/u;
        double pUp   = (R-d)/(u-d);
        double pDown = 1.0 - pUp;
        double dividend_amount = dividendAmounts[0];
        double[] tmp_dividend_times = new double[no_dividends-1];  // temporaries with
        double[] tmp_dividend_amounts = new double[no_dividends-1];  // one less dividend
        for (int i=0;i<(no_dividends-1);++i) {
            tmp_dividend_amounts[i] = dividendAmounts[i+1];
            tmp_dividend_times[i]   = dividendTimes[i+1] - dividendTimes[0];
        }
        double[] prices = new double[steps_before_dividend+1];
        double[] call_values = new double[steps_before_dividend+1];
        prices[0] = S*Math.pow(d, steps_before_dividend);
        for (int i=1; i<=steps_before_dividend; ++i)
            prices[i] = u*u*prices[i-1];
        for (int i=0; i<=steps_before_dividend; ++i) {
            double value_alive = optionPriceCallAmericanDiscreteDividendsBinomial(prices[i]-dividend_amount,
                    K, r, sigma,
                    t-dividendTimes[0],// time after first dividend
                    steps-steps_before_dividend,
                    tmp_dividend_times,
                  tmp_dividend_amounts);
            call_values[i] = Math.max(value_alive,(prices[i]-K));  // compare to exercising now
        }
        for (int step=steps_before_dividend-1; step>=0; --step) {
            for (int i=0; i<=step; ++i)   {
                prices[i] = d*prices[i+1];
                call_values[i] = (pDown*call_values[i]+pUp*call_values[i+1])*Rinv;
                call_values[i] = Math.max(call_values[i], prices[i]-K);
            }
        }
        return call_values[0];
    }

    /**
     * American Option (Put) for dividends with specific (discrete) dollar amounts
     * using binomial approximations
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param t time to maturity 
     * @param steps Number of steps in binomial tree 
     * @param dividendTimes Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year) 
     * @param dividendAmounts Array of dividend amounts for the 'dividendTimes' 
     * @return Option price
     */
    public static double optionPricePutAmericanDiscreteDividendsBinomial(double S, double K,
            double r, double sigma, double t, int steps, double[] dividendTimes, double[] dividendAmounts) {
        // given an amount of dividend, the binomial tree does not recombine, have to
        // start a new tree at each ex-dividend date.
        // do this recursively, at each ex dividend date, at each step, put the
        // binomial formula starting at that point to calculate the value of the live
        // option, and compare that to the value of exercising now.

        int no_dividends = dividendTimes.length;
        if (no_dividends == 0)               // just take the regular binomial
            return optionPricePutAmericanBinomial(S,K,r,sigma,t,steps);
        int steps_before_dividend = (int)(dividendTimes[0]/t*steps);

        double R = Math.exp(r*(t/steps));
        double Rinv = 1.0/R;
        double u = Math.exp(sigma*Math.sqrt(t/steps));
        double uu= u*u;
        double d = 1.0/u;
        double pUp   = (R-d)/(u-d);
        double pDown = 1.0 - pUp;
        double dividend_amount = dividendAmounts[0];
        double[] tmp_dividend_times = new double[no_dividends-1];  // temporaries with
        double[] tmp_dividend_amounts = new double[no_dividends-1];  // one less dividend
        for (int i=0;i<no_dividends-1;++i) {
            tmp_dividend_amounts[i] = dividendAmounts[i+1];
            tmp_dividend_times[i]   = dividendTimes[i+1] - dividendTimes[0];
        }
        double[] prices = new double[steps_before_dividend+1];
        double[] put_values = new double[steps_before_dividend+1];

        prices[0] = S*Math.pow(d, steps_before_dividend);
        for (int i=1; i<=steps_before_dividend; ++i)
            prices[i] = uu*prices[i-1];
        for (int i=0; i<=steps_before_dividend; ++i) {
            double value_alive = optionPricePutAmericanDiscreteDividendsBinomial(
                    prices[i]-dividend_amount, K, r, sigma,
                    t-dividendTimes[0],               // time after first dividend
                    steps-steps_before_dividend,
                    tmp_dividend_times,
                tmp_dividend_amounts);
            // what is the value of keeping the option alive?  Found recursively,
            // with one less dividend, the stock price is current value
            // less the dividend.
            put_values[i] = Math.max(value_alive,(K-prices[i]));  // compare to exercising now
        }
        for (int step=steps_before_dividend-1; step>=0; --step) {
            for (int i=0; i<=step; ++i) {
                prices[i] = d*prices[i+1];
                put_values[i] = (pDown*put_values[i]+pUp*put_values[i+1])*Rinv;
                put_values[i] = Math.max(put_values[i], K-prices[i]);         // check for exercise
            }
        }
        return put_values[0];
    }

    /**
     * American Option (Call) with proportional dividend payments
     * using binomial approximations.
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param time time to maturity 
     * @param noSteps Number of steps in binomial tree 
     * @param dividendTimes Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year) 
     * @param dividendYields Array of dividend yields for the 'dividend_times' 
     * @return Option price
     */
    public static double optionPriceCallAmericanProportionalDividendsBinomial(double S, double K,
            double r, double sigma, double time, int noSteps, double[] dividendTimes, double[] dividendYields) {
        // note that the last dividend date should be before the expiry date, problems if dividend at terminal node
        int no_dividends=dividendTimes.length;
        if (no_dividends == 0) {
            return optionPriceCallAmericanBinomial(S,K,r,sigma,time,noSteps); // price w/o dividends
        }
        double delta_t = time/noSteps;
        double R = Math.exp(r*delta_t);
        double Rinv = 1.0/R;
        double u = Math.exp(sigma*Math.sqrt(delta_t));
        double uu= u*u;
        double d = 1.0/u;
        double pUp   = (R-d)/(u-d);
        double pDown = 1.0 - pUp;
        int[] dividend_steps = new int[no_dividends]; // when dividends are paid
        for (int i=0; i<no_dividends; ++i)
            dividend_steps[i] = (int)(dividendTimes[i]/time*noSteps);

        double[] prices = new double[noSteps+1];
        double[] call_prices = new double[noSteps+1];
        prices[0] = S*Math.pow(d, noSteps); // adjust downward terminal prices by dividends
        for (int i=0; i<no_dividends; ++i)
            prices[0]*=(1.0-dividendYields[i]);
        for (int i=1; i<=noSteps; ++i)
            prices[i] = uu*prices[i-1];
        for (int i=0; i<=noSteps; ++i)
            call_prices[i] = Math.max(0.0, (prices[i]-K));

        for (int step=noSteps-1; step>=0; --step) {
            for (int i=0;i<no_dividends;++i) {   // check whether dividend paid
                if (step==dividend_steps[i]) {
                    for (int j=0;j<=(step+1);++j) {
                        prices[j]*=(1.0/(1.0-dividendYields[i]));
                    }
                }
            }
            for (int i=0; i<=step; ++i)   {
                call_prices[i] = (pDown*call_prices[i]+pUp*call_prices[i+1])*Rinv;
                prices[i] = d*prices[i+1];
                call_prices[i] = Math.max(call_prices[i], prices[i]-K);         // check for exercise
            }
        }
        return call_prices[0];
    }

    /**
     * American Option (Put) with proportional dividend payments
     * using binomial approximations.
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param time time to maturity 
     * @param noSteps Number of steps in binomial tree 
     * @param dividendTimes Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year) 
     * @param dividendYields Array of dividend yields for the 'dividend_times' 
     * @return Option price
     */
    public static double optionPricePutAmericanProportionalDividendsBinomial(double S, double K,
            double r, double sigma, double time, int noSteps, double[] dividendTimes, double[] dividendYields) {
        // when one assume a dividend yield, the binomial tree recombines
        // note that the last dividend date should be before the expiry date
        int no_dividends=dividendTimes.length;
        if (no_dividends == 0)               // just take the regular binomial
            return optionPricePutAmericanBinomial(S,K,r,sigma,time,noSteps);
        double R = Math.exp(r*(time/noSteps));
        double Rinv = 1.0/R;
        double u = Math.exp(sigma*Math.sqrt(time/noSteps));
        double uu= u*u;
        double d = 1.0/u;
        double pUp   = (R-d)/(u-d);
        double pDown = 1.0 - pUp;

        int[] dividend_steps = new int[no_dividends]; // when dividends are paid
        for (int i=0; i<no_dividends; ++i) {
            dividend_steps[i] = (int)(dividendTimes[i]/time*noSteps);
        }

        double[] prices = new double[noSteps+1];
        double[] put_prices = new double[noSteps+1];
        prices[0] = S*Math.pow(d, noSteps);
        for (int i=0; i<no_dividends; ++i)
            prices[0]*=(1.0-dividendYields[i]);
        for (int i=1; i<=noSteps; ++i)
            prices[i] = uu*prices[i-1]; // terminal tree nodes
        for (int i=0; i<=noSteps; ++i)
            put_prices[i] = Math.max(0.0, (K-prices[i]));

        for (int step=noSteps-1; step>=0; --step) {
            for (int i = 0; i < no_dividends; ++i) { // check whether dividend paid
                if (step==dividend_steps[i]) {
                    for (int j=0;j<=(step+1);++j) {
                        prices[j]*=(1.0/(1.0-dividendYields[i]));
                    }
                }
            }
            for (int i=0; i<=step; ++i) {
                prices[i] = d*prices[i+1];
                put_prices[i] = (pDown*put_prices[i]+pUp*put_prices[i+1])*Rinv;
                put_prices[i] = Math.max(put_prices[i], K-prices[i]);         // check for exercise
            }
        }
        return put_prices[0];
    }

    /**
     * American Option (Call) with continuous payouts using binomial
     * approximations.
     * (NOTE: Originally, this method was called: 'option_price_call_american_binomial'
     * that name was already in use and didn't mention the 'payout' properties of
     * the method, so the new name is: 'option_price_call_american_binomial_payout')
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param y continuous payout 
     * @param sigma volatility 
     * @param t time to maturity 
     * @param steps Number of steps in binomial tree 
     * @return Option price
     */
    public static double optionPriceCallAmericanBinomialPayout(double S, double K, double r,
                                                               double y, double sigma, double t, int steps) {
        double R = Math.exp(r*(t/steps));            // interest rate for each step
        double Rinv = 1.0/R;                    // inverse of interest rate
        double u = Math.exp(sigma*Math.sqrt(t/steps));    // up movement
        double uu = u*u;
        double d = 1.0/u;
        double p_up = (Math.exp((r-y)*(t/steps))-d)/(u-d);
        double p_down = 1.0-p_up;
        double[] prices = new double[steps+1];       // price of underlying
        prices[0] = S*Math.pow(d, steps);
        for (int i=1; i<=steps; ++i)
            prices[i] = uu*prices[i-1]; // fill in the endnodes.

        double[] call_values = new double[steps+1];       // value of corresponding call
        for (int i=0; i<=steps; ++i)
            call_values[i] = Math.max(0.0, (prices[i]-K)); // call payoffs at maturity

        for (int step=steps-1; step>=0; --step) {
            for (int i=0; i<=step; ++i) {
                call_values[i] = (p_up*call_values[i+1]+p_down*call_values[i])*Rinv;
                prices[i] = d*prices[i+1];
                call_values[i] = Math.max(call_values[i],prices[i]-K);       // check for exercise
            }
        }
        return call_values[0];
    }

    /**
     * American Option (Put) with continuous payouts using binomial
     * approximations.
     * (NOTE: Originally, this method was called: 'option_price_put_american_binomial'
     * that name was already in use and didn't mention the 'payout' properties of
     * the method, so the new name is: 'option_price_put_american_binomial_payout')
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param y continuous payout 
     * @param sigma volatility 
     * @param t time to maturity 
     * @param steps Number of steps in binomial tree 
     * @return Option price
     */
    public static double optionPricePutAmericanBinomialPayout(double S, double K, double r,
                                                              double y, double sigma, double t, int steps) {
        double R = Math.exp(r*(t/steps));            // interest rate for each step
        double Rinv = 1.0/R;                    // inverse of interest rate
        double u = Math.exp(sigma*Math.sqrt(t/steps));    // up movement
        double uu = u*u;
        double d = 1.0/u;
        double p_up = (Math.exp((r-y)*(t/steps))-d)/(u-d);
        double p_down = 1.0-p_up;
        double[] prices = new double[steps+1];       // price of underlying
        double[] put_values = new double[steps+1];       // value of corresponding put

        prices[0] = S*Math.pow(d, steps);  // fill in the endnodes.
        for (int i=1; i<=steps; ++i)
            prices[i] = uu*prices[i-1];
        for (int i=0; i<=steps; ++i)
            put_values[i] = Math.max(0.0, (K-prices[i])); // put payoffs at maturity
        for (int step=steps-1; step>=0; --step) {
            for (int i=0; i<=step; ++i) {
                put_values[i] = (p_up*put_values[i+1]+p_down*put_values[i])*Rinv;
                prices[i] = d*prices[i+1];
                put_values[i] = Math.max(put_values[i],(K-prices[i]));       // check for exercise
            }
        }
        return put_values[0];
    }

    /**
     * European Option (Call) using binomial approximations
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param t time to maturity 
     * @param steps Number of steps in binomial tree 
     * @return Option price
     */
    public static double optionPriceCallEuropeanBinomial(double S, double K, double r,
                                                         double sigma, double t, int steps) {
        double R = Math.exp(r*(t/steps));            // interest rate for each step
        double Rinv = 1.0/R;                    // inverse of interest rate
        double u = Math.exp(sigma*Math.sqrt(t/steps));    // up movement
        double uu = u*u;
        double d = 1.0/u;
        double p_up = (R-d)/(u-d);
        double p_down = 1.0-p_up;
        double[] prices = new double[steps+1];       // price of underlying
        prices[0] = S*Math.pow(d, steps);  // fill in the endnodes.
        for (int i=1; i<=steps; ++i)
            prices[i] = uu*prices[i-1];
        double[] call_values = new double[steps+1];       // value of corresponding call
        for (int i=0; i<=steps; ++i)
            call_values[i] = Math.max(0.0, (prices[i]-K)); // call payoffs at maturity
        for (int step=steps-1; step>=0; --step) {
            for (int i=0; i<=step; ++i) {
                call_values[i] = (p_up*call_values[i+1]+p_down*call_values[i])*Rinv;
            }
        }
        return call_values[0];
    }

    /**
     * European Option (Put) using binomial approximations
     *
     * @param S spot (underlying) price 
     * @param K strike (exercise) price 
     * @param r interest rate 
     * @param sigma volatility 
     * @param t time to maturity 
     * @param steps Number of steps in binomial tree 
     * @return Option price
     */
    public static double optionPricePutEuropeanBinomial(double S, double K, double r,
                                                        double sigma, double t, int steps) {
        double R = Math.exp(r*(t/steps));            // interest rate for each step
        double Rinv = 1.0/R;                    // inverse of interest rate
        double u = Math.exp(sigma*Math.sqrt(t/steps));    // up movement
        double uu = u*u;
        double d = 1.0/u;
        double p_up = (R-d)/(u-d);
        double p_down = 1.0-p_up;
        double[] prices = new double[steps+1];       // price of underlying
        prices[0] = S*Math.pow(d, steps);  // fill in the endnodes.
        for (int i=1; i<=steps; ++i)
            prices[i] = uu*prices[i-1];
        double[] put_values = new double[steps+1];       // value of corresponding put
        for (int i=0; i<=steps; ++i)
            put_values[i] = Math.max(0.0, (K-prices[i])); // put payoffs at maturity
        for (int step=steps-1; step>=0; --step) {
            for (int i=0; i<=step; ++i) {
                put_values[i] = (p_up*put_values[i+1]+p_down*put_values[i])*Rinv;
            }
        }
        return put_values[0];
    }
}
