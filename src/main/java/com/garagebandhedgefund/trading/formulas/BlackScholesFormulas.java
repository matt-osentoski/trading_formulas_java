package com.garagebandhedgefund.trading.formulas;

import com.garagebandhedgefund.trading.model.OptionPartials;

/**
 * Created by Matt Osentoski on 8/1/2014.
 *
 * This class contains formulas based on the Black Scholes model
 *
 * Converted to C# from "Financial Numerical Recipes in C" by:
 * Bernt Arne Odegaard
 * http://finance.bi.no/~bernt/gcc_prog/index.html
 */
public class BlackScholesFormulas {
    /**
     * Normal distribution
     * @param z Value to test
     * @return Normal distribution
     */
    public static double normDist(double z) {
        return (1.0/Math.sqrt(2.0 * Math.PI))* Math.exp(-0.5 * z * z);
    }

    /**
     * Cumulative normal distribution
     * Abramowiz and Stegun approximation (1964)
     * @param z Value to test
     * @return Cumulative normal distribution
     */
    public static double cumulativeNormDist(double z) {
        if (z > 6.0) { return 1.0; } // this guards against overflow
        if (z < -6.0) { return 0.0; }

        double b1 = 0.31938153;
        double b2 = -0.356563782;
        double b3 = 1.781477937;
        double b4 = -1.821255978;
        double b5 = 1.330274429;
        double p = 0.2316419;
        double c2 = 0.3989423;

        double a = Math.abs(z);
        double t = 1.0 / (1.0 + a * p);
        double b = c2 * Math.exp((-z) * (z / 2.0));
        double n = ((((b5 * t + b4) * t + b3) * t + b2) * t + b1) * t;
        n = 1.0 - b * n;
        if (z < 0.0)
            n = 1.0 - n;
        return n;
    }

    /**
     * Black Scholes formula (Call)
     * Black and Scholes (1973) and Merton (1973)
     *
     * @param S spot (underlying) price
     * @param K strike (exercise) price
     * @param r interest rate
     * @param sigma volatility
     * @param time time to maturity
     * @return Option price
     */
    public static double optionPriceCallBlackScholes(double S, double K, double r, double sigma, double time) {
        double timeSqrt = Math.sqrt(time);
        double d1 = (Math.log(S / K)+r*time)/(sigma*timeSqrt)+0.5*sigma*timeSqrt;
        double d2 = d1-(sigma*timeSqrt);
        return S * cumulativeNormDist(d1) - K * Math.exp(-r * time) * cumulativeNormDist(d2);
    }

    /**
     * Black Scholes formula (Put)
     * Black and Scholes (1973) and Merton (1973)
     * 
     * @param S spot (underlying) price
     * @param K strike (exercise) price
     * @param r interest rate
     * @param sigma volatility
     * @param time time to maturity
     * @return Option price
     */
    public static double optionPricePutBlackScholes(double S, double K, double r, double sigma, double time) {
        double timeSqrt = Math.sqrt(time);
        double d1 = (Math.log(S / K)+r*time)/(sigma*timeSqrt) + 0.5*sigma*timeSqrt;
        double d2 = d1-(sigma*timeSqrt);
        return K * Math.exp(-r * time) * cumulativeNormDist(-d2) - S * cumulativeNormDist(-d1);
    }

    /** 
     * Delta of the Black Scholes formula (Call)
     * 
     * @param S spot (underlying) price
     * @param K strike (exercise) price
     * @param r interest rate
     * @param sigma volatility
     * @param time time to maturity
     * @return Delta of the option
     */
    public static double optionPriceDeltaCallBlackScholes(double S, double K, double r, double sigma, double time) {
        double timeSqrt = Math.sqrt(time);
        double d1 = (Math.log(S / K) + r * time) / (sigma * timeSqrt) + 0.5 * sigma * timeSqrt;
        double delta = cumulativeNormDist(d1);
        return delta;
    }

    /** <summary>
     * Delta of the Black Scholes formula (Put)
     * 
     * @param S spot (underlying) price
     * @param K strike (exercise) price
     * @param r interest rate
     * @param sigma volatility
     * @param time time to maturity
     * @return Delta of the option
     */
    public static double optionPriceDeltaPutBlackScholes(double S, double K, double r, double sigma, double time) {
        double timeSqrt = Math.sqrt(time);
        double d1 = (Math.log(S / K) + r * time) / (sigma * timeSqrt) + 0.5 * sigma * timeSqrt;
        double delta = -cumulativeNormDist(-d1);
        return delta;
    }

    /**
     * Calculates implied volatility for the Black Scholes formula using
     * binomial search algorithm
     * (NOTE: In the original code a large negative number was used as an
     * exception handling mechanism.  This has been replace with a generic
     * 'Exception' that is thrown.  The original code is in place and commented
     * if you want to use the pure version of this code)
     * 
     * @param S spot (underlying) price
     * @param K strike (exercise) price
     * @param r interest rate
     * @param time time to maturity
     * @param optionPrice The price of the option
     * @return Sigma (implied volatility)
     */
    public static double optionPriceImpliedVolatilityCallBlackScholesBisections(double S, double K, double r,
                                                                                double time, double optionPrice)
                                                                                throws Exception {
        // check for arbitrage violations.
        if (optionPrice < 0.99 * (S - K * Math.exp(-time * r))) {
            return 0.0;  // Option price is too low if this happens
        }

        // simple binomial search for the implied volatility.
        // relies on the value of the option increasing in volatility
        double ACCURACY = 1.0e-5; // make this smaller for higher accuracy
        int MAX_ITERATIONS = 100;
        double HIGH_VALUE = 1e10;
        //const double ERROR = -1e40;  // <--- original code

        // want to bracket sigma. first find a maximum sigma by finding a sigma
        // with a estimated price higher than the actual price.
        double sigmaLow = 1e-5;
        double sigmaHigh = 0.3;
        double price = optionPriceCallBlackScholes(S, K, r, sigmaHigh, time);
        while (price < optionPrice) {
            sigmaHigh = 2.0 * sigmaHigh; // keep doubling.
            price = optionPriceCallBlackScholes(S, K, r, sigmaHigh, time);
            if (sigmaHigh > HIGH_VALUE) {
                //return ERROR; // panic, something wrong.     // <--- original code
                throw new Exception("panic, something wrong."); // Comment this line if you uncomment the line above
            }
        }
        for (int i = 0; i < MAX_ITERATIONS; i++) {
            double sigma = (sigmaLow + sigmaHigh) * 0.5;
            price = optionPriceCallBlackScholes(S, K, r, sigma, time);
            double test = (price - optionPrice);
            if (Math.abs(test) < ACCURACY)
                return sigma;
            if (test < 0.0)
                sigmaLow = sigma;
            else
                sigmaHigh = sigma;
        }
        //return ERROR;      // <--- original code
        throw new Exception("An error occurred"); // Comment this line if you uncomment the line above
    }

    /**
     * Calculates implied volatility for the Black Scholes formula using
     * the Newton-Raphson formula
     * (NOTE: In the original code a large negative number was used as an
     * exception handling mechanism.  This has been replace with a generic
     * 'Exception' that is thrown.  The original code is in place and commented
     * if you want to use the pure version of this code)
     * 
     * @param S spot (underlying) price
     * @param K strike (exercise) price
     * @param r interest rate
     * @param time time to maturity
     * @param optionPrice The price of the option
     * @return Sigma (implied volatility)
     */
    public static double optionPriceImpliedVolatilityCallBlackScholesNewton(double S, double K, double r,
                                                                            double time, double optionPrice)
                                                                            throws Exception {
        // check for arbitrage violations. Option price is too low if this happens
        if (optionPrice < 0.99 * (S - K * Math.exp(-time * r))) {
            return 0.0;
        }

        int MAX_ITERATIONS = 100;
        double ACCURACY = 1.0e-5;
        double tSqrt = Math.sqrt(time);

        double sigma = (optionPrice / S) / (0.398 * tSqrt);    // find initial value
        for (int i = 0; i < MAX_ITERATIONS; i++) {
            double price = optionPriceCallBlackScholes(S, K, r, sigma, time);
            double diff = optionPrice - price;
            if (Math.abs(diff) < ACCURACY)
                return sigma;
            double d1 = (Math.log(S / K) + r * time) / (sigma * tSqrt) + 0.5 * sigma * tSqrt;
            double vega = S * tSqrt * normDist(d1);
            sigma = sigma + diff / vega;
        }
        //return -99e10;  // something screwy happened, should throw exception  // <--- original code
        throw new Exception("An error occurred"); // Comment this line if you uncomment the line above
    }

    /**
     * Calculate partial derivatives for a Black Scholes Option (Call)
     * 
     * @param S spot (underlying) price
     * @param K strike (exercise) price
     * @param r interest rate
     * @param sigma volatility
     * @param time time to maturity
     * @return OptionPartials Partial derivatives for this option
     */
    public static OptionPartials optionPricePartialsCallBlackScholes( double S, double K, double r, double sigma,
                                                            double time) {
        double timeSqrt = Math.sqrt(time);
        double d1 = (Math.log(S / K)+r*time)/(sigma*timeSqrt) + 0.5*sigma*timeSqrt;
        double d2 = d1-(sigma*timeSqrt);
        double delta = cumulativeNormDist(d1);
        double gamma = normDist(d1) / (S * sigma * timeSqrt);
        double theta = -(S * sigma * normDist(d1)) / (2 * timeSqrt) - r * K * Math.exp(-r * time) * cumulativeNormDist(d2);
        double vega = S * timeSqrt * normDist(d1);
        double rho = K * time * Math.exp(-r * time) * cumulativeNormDist(d2);
        OptionPartials partials = new OptionPartials(delta,gamma,theta,vega,rho);
        return partials;
    }

    /**
     * Calculate partial derivatives for a Black Scholes Option (Put)
     * 
     * @param S spot (underlying) price
     * @param K strike (exercise) price
     * @param r interest rate
     * @param sigma volatility
     * @param time time to maturity
     * @return OptionPartials Partial derivatives for this option
     */
    public static OptionPartials optionPricePartialsPutBlackScholes(double S, double K, double r, double sigma,
                                                          double time) {
        double timeSqrt = Math.sqrt(time);
        double d1 = (Math.log(S / K) + r * time) / (sigma * timeSqrt) + 0.5 * sigma * timeSqrt;
        double d2 = d1 - (sigma * timeSqrt);
        double delta = -cumulativeNormDist(-d1);
        double gamma = normDist(d1) / (S * sigma * timeSqrt);
        double theta = -(S * sigma * normDist(d1)) / (2 * timeSqrt) + r * K * Math.exp(-r * time) * cumulativeNormDist(-d2);
        double vega = S * timeSqrt * normDist(d1);
        double rho = -K * time * Math.exp(-r * time) * cumulativeNormDist(-d2);
        OptionPartials partials = new OptionPartials(delta,gamma,theta,vega,rho);
        return partials;
    }

    /**
     * European option (Call) with a continuous payout.
     * The continuous payout would be for fees associated with the asset.
     * For example, storage costs.
     * 
     * @param S spot (underlying) price
     * @param X strike (exercise) price
     * @param r interest rate
     * @param q yield on underlying
     * @param sigma volatility
     * @param time time to maturity
     * @return Option price
     */
    public static double optionPriceEuropeanCallPayout(double S, double X, double r, double q,
                                                       double sigma, double time) {
        double sigmaSqr = Math.pow(sigma, 2);
        double timeSqrt = Math.sqrt(time);
        double d1 = (Math.log(S / X) + (r - q + 0.5 * sigmaSqr) * time) / (sigma * timeSqrt);
        double d2 = d1 - (sigma * timeSqrt);
        double callPrice = S * Math.exp(-q * time) * cumulativeNormDist(d1) - X * Math.exp(-r * time) * cumulativeNormDist(d2);
        return callPrice;
    }

    /**
     * European option (Put) with a continuous payout.
     * The continuous payout would be for fees associated with the asset.
     * For example, storage costs.
     * 
     * @param S spot (underlying) price
     * @param K strike (exercise) price
     * @param r interest rate
     * @param q yield on underlying
     * @param sigma volatility
     * @param time time to maturity
     * @return Option price
     */
    public static double optionPriceEuropeanPutPayout(double S, double K, double r, double q,
                                                      double sigma, double time) {
        double sigmaSqr = Math.pow(sigma, 2);
        double timeSqrt = Math.sqrt(time);
        double d1 = (Math.log(S / K) + (r - q + 0.5 * sigmaSqr) * time) / (sigma * timeSqrt);
        double d2 = d1 - (sigma * timeSqrt);
        double putPrice = K * Math.exp(-r * time) * cumulativeNormDist(-d2) - S * Math.exp(-q * time) * cumulativeNormDist(-d1);
        return putPrice;
    }

    /**
     * European option for known dividends (Call)
     * 
     * @param S spot (underlying) price
     * @param K strike (exercise) price
     * @param r interest rate
     * @param sigma volatility
     * @param timeToMaturity time to maturity
     * @param dividendTimes Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)
     * @param dividendAmounts Array of dividend amounts for the 'dividendTimes'
     * @return Option price
     */
    public static double optionPriceEuropeanCallDividends(double S, double K, double r,
                                                          double sigma, double timeToMaturity, double[] dividendTimes, double[] dividendAmounts) {
        double adjustedS = S;
        for (int i = 0; i < dividendTimes.length; i++) {
            if (dividendTimes[i] <= timeToMaturity) {
                adjustedS -= dividendAmounts[i] * Math.exp(-r * dividendTimes[i]);
            }
        }
        return optionPriceCallBlackScholes(adjustedS, K, r, sigma, timeToMaturity);
    }

    /**
     * European option for known dividends (Put)
     * 
     * @param S spot (underlying) price
     * @param K strike (exercise) price
     * @param r interest rate
     * @param sigma volatility
     * @param timeToMaturity time to maturity
     * @param dividendTimes Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)
     * @param dividendAmounts Array of dividend amounts for the 'dividendTimes'
     * @return Option price
     */
    public static double optionPriceEuropeanPutDividends(double S, double K, double r,
                                                         double sigma, double timeToMaturity, double[] dividendTimes, double[] dividendAmounts) {
        double adjustedS = S;
        for (int i = 0; i < dividendTimes.length; i++) {
            if (dividendTimes[i]<=timeToMaturity) {
                adjustedS -= dividendAmounts[i] * Math.exp(-r * dividendTimes[i]);
            }
        }
        return optionPricePutBlackScholes(adjustedS, K, r, sigma, timeToMaturity);

    }

}

