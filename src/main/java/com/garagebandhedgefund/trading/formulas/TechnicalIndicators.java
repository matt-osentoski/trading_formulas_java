package com.garagebandhedgefund.trading.formulas;

import com.garagebandhedgefund.trading.model.PivotPoint;
import com.garagebandhedgefund.trading.model.StockPrice;
import com.garagebandhedgefund.trading.vo.BollingerBandVO;
import com.garagebandhedgefund.trading.vo.MomentumVO;
import com.garagebandhedgefund.trading.vo.StochasticVO;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 8/22/14
 * Time: 2:02 PM
 */
public class TechnicalIndicators {
    /**
     * Returns a pivot point object
     * @param high High price for a specific period
     * @param low Low price for a specific period
     * @param close Closing price for a specific period
     * @return PivotPoint A pivot Point object.
     */
    public static PivotPoint getPivotPoint(double high, double low, double close) {
        PivotPoint pivotPoint = new PivotPoint();

        double range = high - low;
        double p = (high + low + close) / 3;

        pivotPoint.setP(p);
        pivotPoint.setR1((p * 2) - low);
        pivotPoint.setR2(p + (range));
        pivotPoint.setR3(p + (range * 2));
        pivotPoint.setS1((p * 2) - high);
        pivotPoint.setS2(p - (range));
        pivotPoint.setS3(p - (range * 2));

        return pivotPoint;
    }

    /**
     * Returns a pivot point object
     * @param high High price for a specific period
     * @param low Low price for a specific period
     * @param close Closing price for a specific period
     * @param open (Optional) The opening price for a specific period
     * @return PivotPoint A pivot Point object.
     */
    public static PivotPoint getPivotPoint(double high, double low, double close, double open) {
        PivotPoint pivotPoint = new PivotPoint();

        double range = high - low;
        double p = (high + low + close + open) / 4;

        pivotPoint.setP(p);
        pivotPoint.setR1((p * 2) - low);
        pivotPoint.setR2(p + (range));
        pivotPoint.setR3(p + (range * 2));
        pivotPoint.setS1((p * 2) - high);
        pivotPoint.setS2(p - (range));
        pivotPoint.setS3(p - (range * 2));

        return pivotPoint;
    }

    /**
     * Return the Simple moving average for a particular period
     * @param stockPrices An array of StockPrice objects
     * @param idx The index of the 'stock_prices_array' where the simple moving average applies
     * @param period The period of this moving average, for example 20 day moving average.
     * @return double Returns the simple moving average.
     */

    public static double getSMA(List<StockPrice> stockPrices, int idx, int period) {

        if (idx >= (period - 1)) {
            int startMaArray = idx- (period -1);
            int endMaArray = startMaArray + period;
            List<StockPrice> maArray = stockPrices.subList(startMaArray, endMaArray);

            double sum = 0.0f;
            for (StockPrice sp : maArray) {
                sum += sp.getClosingPrice();
            }

            double ma = sum / period;
            return ma;
        } else {
            return 0.0f;
        }

    }

    /**
     * Return the Exponential moving average for a particular period
     * @param stockPrices An array of StockPrice objects
     * @param idx The index of the 'stock_prices_array' where the simple moving average applies
     * @param period The period of this moving average, for example 20 day moving average.
     * @param previousEMA The previous EMA that was calculated.
     * @return double Returns the simple moving average.
     */

    public static double getEMA(List<StockPrice> stockPrices, int idx, int period, double previousEMA) {

        double smoothingConstant = 2.0 / (period +1);

        if (idx == (period - 1)) {
            int startMaArray = idx- (period -1);
            int endMaArray = startMaArray + period;
            List<StockPrice> maArray = stockPrices.subList(startMaArray, endMaArray);

            double sum = 0.0f;
            for (StockPrice sp : maArray) {
                sum += sp.getClosingPrice();
            }

            double ema = sum / period;
            return ema;
        } else if (idx > (period-1) && previousEMA != 0) {
            double currentClose = stockPrices.get(idx).getClosingPrice();
            double ema = ( (smoothingConstant * (currentClose - previousEMA)) + previousEMA );
            return ema;
        } else {
            return 0;
        }

    }

    /**
     * Returns the Moving Average Convergence/Divergence (MACD)
     * @param ema12 Exponential 12 day moving average
     * @param ema26 Exponential 26 day moving average
     * @return double Returns the MACD value
     */
    public static double getMACD(double ema12, double ema26) {
        return (ema12 - ema26);
    }

    /**
     * Return the MACD Exponential moving average for a particular period
     * @param stockPrices An array of StockPrice objects
     * @param idx The index of the 'stock_prices_array' where the simple moving average applies
     * @param period The period of this moving average, for example 9 day moving average.
     * @param previousMACDEMA The previous MACD EMA that was calculated.
     * @return double Returns the simple moving average.
     */

    public static double getMACDEMA(List<StockPrice> stockPrices, int idx, int period, double previousMACDEMA) {

        // First preprocess the stock prices
        List<MomentumVO> stockPricesProcessed = preprocessMACDPPOEMA(stockPrices);

        double smoothingConstant = 2.0 / (period +1);
        List<MomentumVO> maArray = new ArrayList<MomentumVO>();

        if (idx == (period - 1)) {
            int startMaArray = idx- (period -1);
            int endMaArray = startMaArray + period;
            maArray = stockPricesProcessed.subList(startMaArray, endMaArray);

            double sum = 0.0;
            for (MomentumVO sp : maArray) {
                sum += sp.getMacd_12_26();
            }

            double ema = sum / period;
            return ema;
        } else if (idx > (period-1)) {
            double currentClose = stockPricesProcessed.get(idx).getMacd_12_26();
            double ema = ( (smoothingConstant * (currentClose - previousMACDEMA)) + previousMACDEMA );
            return ema;
        } else {
            return 0;
        }

    }

    /**
     * Returns the Moving Average Convergence/Divergence (MACD) Histogram
     * @param macd The MACD value
     * @param macdEma9 Exponential 9 day moving average
     * @return double Returns the MACD Histogram value
     */
    public static double getMACDHistogram(double macd, double macdEma9) {
        return (macd - macdEma9);
    }

    /**
     * Return the Volume Simple moving average for a particular period
     * @param stockPrices An array of StockPrice objects
     * @param idx The index of the 'stock_prices_array' where the simple moving average applies
     * @param period The period of this moving average, for example 20 day moving average.
     * @return Long Returns the simple moving average for volumes.
     */

    public static long getVolSMA(List<StockPrice> stockPrices, int idx, int period) {

        List<StockPrice> maArray = new ArrayList<StockPrice>();

        if (idx >= (period - 1)) {
            int startMaArray = idx- (period -1);
            int endMaArray = startMaArray + period;
            maArray = stockPrices.subList(startMaArray, endMaArray);

            long sum = 0;
            for (StockPrice sp : maArray) {
                sum += sp.getVolume();
            }

            long ma = sum / period;
            return ma;
        } else {
            return 0;
        }
    }

    /**
     * Returns the Percentage Price Oscillator (PPO)
     * @param ema12 Exponential 12 day moving average
     * @param ema26 Exponential 26 day moving average
     * @return double Returns the PPO value
     */
    public static double getPPO(double ema12, double ema26) {
        if (ema26 == 0) {
            return 0;
        }
        return (ema12 - ema26) / ema26;
    }

    /**
     * Return the PPO Exponential moving average for a particular period
     * @param stockPrices An array of StockPrice objects
     * @param idx The index of the 'stock_prices_array' where the simple moving average applies
     * @param period The period of this moving average, for example 9 day moving average.
     * @param previousPPOEMA The previous PPO EMA that was calculated.
     * @return double Returns the simple moving average.
     */

    public static double getPPOEMA(List<StockPrice> stockPrices, int idx, int period, double previousPPOEMA) {

        // First preprocess the stock prices
        List<MomentumVO> stockPricesProcessed = preprocessMACDPPOEMA(stockPrices);

        double smoothingConstant = 2.0 / (period +1);
        List<MomentumVO> maArray = new ArrayList<MomentumVO>();

        if (idx == (period - 1)) {
            int startMaArray = idx- (period -1);
            int endMaArray = startMaArray + period;
            maArray = stockPricesProcessed.subList(startMaArray, endMaArray);

            double sum = 0.0;
            for (MomentumVO sp : maArray) {
                sum += sp.getPpo_12_26();
            }

            double ema = sum / period;
            return ema;
        } else if (idx > (period-1)) {
            double currentClose = stockPricesProcessed.get(idx).getPpo_12_26();
            double ema = ( (smoothingConstant * (currentClose - previousPPOEMA)) + previousPPOEMA );
            return ema;
        } else {
            return 0;
        }

    }

    /**
     * Returns the Percentage Price Oscillator (PPO) Histogram
     * @param ppo The PPO value
     * @param ppoEma9 Exponential 9 day moving average
     * @return double Returns the PPO Histogram value
     */
    public static double getPPOHistogram(double ppo, double ppoEma9) {
        return (ppo - ppoEma9);
    }

    /**
     * Returns the Relative Strength Index (RSI)
     * @param stockPrices An array of StockPrice objects
     * @param idx The index of the 'stock_prices_array' of this RSI value
     * @param period The period of this RSI calculation, for example 14.
     * @return double Returns the RSI value
     */
    public static double getRSI(List<StockPrice> stockPrices, int idx, int period) {
        if (period == 0) {
            period = 14; // Period should default to 14
        }

        if (idx >= period) {
			/*
			 * The RSI of the first calculation is different than subsequent calculations
			 * The first calculation of RSI begins when you have a number of closing prices equal to the
			 * period you're looking at.  In the default case, after 14 closing prices (Actually this will
			 * be after 15 closing prices, because you have to compare the first closing price with something
			 * to determine if there was a gain or loss.
			 */

            List<StockPrice> gainLossList = stockPrices.subList(1, (period +1));
            List<Double> gains = new ArrayList<Double>();
            List<Double> losses = new ArrayList<Double>();

            double previousPrice = stockPrices.get(0).getClosingPrice();
            double previousAvgGain = 0;
            double previousAvgLoss = 0;

            for (int x=0; x<gainLossList.size(); x++) {
                StockPrice stockPrice = gainLossList.get(x);
                double priceChange = stockPrice.getClosingPrice() - previousPrice;
                if (priceChange < 0) {
                    losses.add(new Double(Math.abs(priceChange)));
                } else {
                    gains.add(new Double(priceChange));
                }
                // Set this price as the new previous_price for the next iteration
                previousPrice = stockPrice.getClosingPrice();
            } // end gainLostList...

            double sum = 0.0f;
            for (Double g : gains) {
                sum += g.doubleValue();
            }
            double avgGain = sum / period;

            sum = 0.0f;
            for (Double l : losses) {
                sum += l.doubleValue();
            }
            double avgLoss = sum / period;

            previousAvgGain = avgGain;
            previousAvgLoss = avgLoss;

            double firstRS = avgGain / avgLoss;
            double firstRSI = 0;

            if (avgLoss == 0) {
                firstRSI = 0;
            } else {
                firstRSI = (100 - (100 / (1 + firstRS) ));
            }

            if (idx == period) {
                return firstRSI;
            }

            // Now that the first RSI value has been established, the running RSI calculation can begin

            for (int x=0; x<stockPrices.size(); x++) {
                if (x<=period) {
                    continue;
                }
                StockPrice stockPrice = stockPrices.get(x);
                double priceChange = stockPrice.getClosingPrice() - previousPrice;

				/*
				 *  Now that the price change has been calculated, set the current price
				 *  to the previous price for the next iteration.
				 */
                previousPrice = stockPrice.getClosingPrice();

                double gain = 0;
                double loss = 0;
                if (priceChange < 0) {
                    loss = Math.abs(priceChange); // losses are represented as positive numbers
                } else {
                    gain = priceChange;
                }

                avgGain = ((previousAvgGain * (period-1)) + gain) / period;
                avgLoss = ((previousAvgLoss * (period-1)) + loss) / period;

				/*
				 * Now that the average gain/loss has been established, set the
				 * current gain/loss to the previous_avg_gain/loss for the next iteration.
				 */
                previousAvgGain = avgGain;
                previousAvgLoss = avgLoss;

                double rs = avgGain / avgLoss;
                double rsi = 0;
                if (avgLoss == 0) {
                    rsi = 100;
                } else {
                    rsi = (100 - (100 / (1 + rs) ));
                }

                if (idx == x) {
                    return rsi;
                }
            } // end stockPrices...
            return 0;  // If for some reason a value hasn't been returned, set RSI to 0
        } else {  // end idx >= period...
            return 0;
        }
    }

    /**
     * Returns the Stochastics
     * @param stockPrices An array of StockPrice objects
     * @param idx The index of the 'stock_prices_array' of this RSI value
     * @param kPeriod The period for %K (example 10)
     * @param dPeriod The period for %D (example 3)
     * @return StochasticVO Value object that stores k, d, k (slow), d (slow).
     */
    public static  StochasticVO getStochastics(List<StockPrice> stockPrices, int idx,
                                       int kPeriod, int dPeriod) {
        // Set defaults if necessary.
        if (kPeriod == 0)
            kPeriod = 10;
        if (dPeriod == 0)
            dPeriod = 3;

        if (idx >= (kPeriod -1) ) {
			/*
			 * The array below will keep track of all numerators and denominators used
			 *  to calculate %K, these values will then be used to calculate %D
			 */

            List<Double> fastKList = new ArrayList<Double>();

            // This array keeps track of slow %K to later calculate slow %D
            List<Double> slowKList = new ArrayList<Double>();

            for (int x =0; x<stockPrices.size(); x++) {
                if (x>= (kPeriod - 1)) {
                    StockPrice stockPrice = stockPrices.get(x);

                    int startPeriodList = x - (kPeriod - 1);
                    int endPeriodList = startPeriodList + kPeriod;
                    List<StockPrice> periodList = stockPrices.subList(startPeriodList, endPeriodList);
                    double maxHigh = Collections.max(periodList, new MaxHighComparator()).getHighPrice();
                    double minLow = Collections.min(periodList, new MinLowComparator()).getLowPrice();

                    double num = stockPrice.getClosingPrice() - minLow;
                    double den = maxHigh - minLow;
                    double k = 0;
                    if ( num == 0 ) {
                        k = 0;
                    } else {
                        k = 100 * (num / den);
                    }
                    fastKList.add(new Double(k));

                    double d = 0;

                    // If there is enough old data, calculate %D
                    if (fastKList.size() >= dPeriod) {
                        int tmpFastKStart = fastKList.size() - dPeriod;
                        int tmpFastKStop = tmpFastKStart + dPeriod;
                        List<Double> fastKListTrimmed = fastKList.subList(tmpFastKStart, tmpFastKStop);

                        double sum = 0;
                        for (Double f : fastKListTrimmed) {
                            sum += f.doubleValue();
                        }
                        if (sum == 0)
                        {
                            d = 0;
                        } else {
                            d = sum / dPeriod;
                        }
                    } else {
                        d = 0;
                    }

                    // Calculate Stochastics Slow
                    // # %D (Fast) is the %K slow
                    double kSlow = d;
                    if (kSlow != 0) {
                        slowKList.add(new Double(kSlow));
                    }

                    double dSlow = 0;

                    // If there is enough old data, calculate %D (slow)
                    if (slowKList.size() == dPeriod) {
                        double sum = 0;
                        for (Double f : slowKList) {
                            sum += f.doubleValue();
                        }
                        if (sum == 0) {
                            dSlow = 0;
                        } else {
                            dSlow = sum / dPeriod;
                        }
                    } else if (slowKList.size() > dPeriod) {
                        int tmpSlowKStart = slowKList.size() - dPeriod;
                        int tmpSlowKStop = tmpSlowKStart + dPeriod;
                        List<Double> tmpSlowKList = slowKList.subList(tmpSlowKStart, tmpSlowKStop);
                        double sum = 0;
                        for (Double f : tmpSlowKList) {
                            sum += f.doubleValue();
                        }
                        if (sum == 0) {
                            dSlow = 0;
                        } else {
                            dSlow = sum / dPeriod;
                        }
                    } else {
                        dSlow = 0;
                    }

                    // If this is the index we want, return all the values
                    if (idx ==x) {
                        StochasticVO svo = new StochasticVO();
                        svo.setK(k);
                        svo.setD(d);
                        svo.setkSlow(kSlow);
                        svo.setdSlow(dSlow);
                        return svo;
                    }

                } //end x> (kPeriod -1...
            } //end for stockPrices...
        } //end if(idx > kPeriod...

        StochasticVO svo = new StochasticVO();
        svo.setK(0);
        svo.setD(0);
        svo.setkSlow(0);
        svo.setdSlow(0);
        return svo;
    }


    /** Returns the Average True Range
     * @param stockPrices An array of StockPrice objects
     * @param idx The index of the 'stock_prices_array' of this RSI value
     * @param previous_atr The previous ATR value.
     * @param period The period for the ATR
     * @return  double Returns the Average True Range
     */
    public static double getATR(List<StockPrice> stockPrices, int idx, double previous_atr, int period) {
        if (period == 0) {
            period = 14; // Period should default to 14
        }

        double prev_close = 0;
        List<Double> daily_tr_arr = new ArrayList<Double>();

        if (idx >= period -1) {
            prev_close = 0;
            daily_tr_arr = new ArrayList<Double>();
            for (int x=0; x<stockPrices.size(); x++) {
                double daily_tr = 0;

                // For the first entry, the Daily TR is the 'High' - 'Low'
                if (x==0) {
                    daily_tr = stockPrices.get(x).getHighPrice() - stockPrices.get(x).getLowPrice();
                } else {
                    double high_low = stockPrices.get(x).getHighPrice() - stockPrices.get(x).getLowPrice();
                    double high_prev_close = Math.abs(stockPrices.get(x).getHighPrice() - prev_close);
                    double low_prev_close = Math.abs(stockPrices.get(x).getLowPrice() - prev_close);
                    double[] max_tmp_arr = {high_low, high_prev_close, low_prev_close};
                    daily_tr = getMaxValue(max_tmp_arr);
                }
                // Make the prev close, the current prev close and add the TR to the TR array.
                prev_close = stockPrices.get(x).getClosingPrice();
                daily_tr_arr.add(new Double(daily_tr));

                // Slice off the daily_tr_arr to the period and generate an average
                // if the index is the period value, otherwise, use the smoothed out average.
                double atr = 0;
                if (idx == period - 1 && x == idx) {
                    int start_tmp_daily_tr_arr = daily_tr_arr.size() - period;
                    List<Double> tmp_daily_tr_arr = daily_tr_arr.subList(start_tmp_daily_tr_arr, period);
                    double sum = 0.0f;
                    for (Double dailyTr : tmp_daily_tr_arr) {
                        sum += dailyTr.doubleValue();
                    }
                    atr = sum / period;
                    return atr;
                } else if (idx > period - 1 && x == idx) {
                    atr = (previous_atr * (period -1) + daily_tr) / period;
                    return atr;
                } // (if (idx == x)...
            } // for (int x=0; x<stockPrices...
        } else {
            return 0.0f;
        }
        return 0.0f;
    }

    /**
     * Returns the Bollinger Bands
     * Based on a spreadsheet from:
     * http://stockcharts.com/school/doku.php?id=chart_school:technical_indicators:bollinger_bands
     *
     * @param stockPrices A collection of stockPrice objects
     * @param idx The current index of the stock prices to analyze
     * @param period The moving average period.  This defaults to 20
     * @param stdDevMultiplier The standard deviation multiplier for the bands.  This defaults to 2.
     * @return
     */
    public static BollingerBandVO getBollingerBands(List<StockPrice> stockPrices, int idx, int period,
                                                   double stdDevMultiplier) {
        // Set defaults
        if (period == 0) period =20;
        if (stdDevMultiplier == 0) stdDevMultiplier = 2;

        BollingerBandVO bb = new BollingerBandVO();
        if (idx >= period -1) {
            double sma = TechnicalIndicators.getSMA(stockPrices, idx, period);
            // Get the Standard deviation based on the period and index
            int startMaArray = idx- (period -1);
            int endMaArray = startMaArray + period;
            List<StockPrice> subArray = stockPrices.subList(startMaArray, endMaArray);
            // Build an array of just closing prices
            double[] closingPrices = new double[subArray.size()];
            for (int x=0; x<subArray.size(); x++) {
                closingPrices[x] = subArray.get(x).getClosingPrice();
            }
            StandardDeviation sd = new StandardDeviation(false);
            double stdDev = sd.evaluate(closingPrices);
            bb.setUpperBand(sma+stdDev*stdDevMultiplier);
            bb.setLowerBand(sma-stdDev*stdDevMultiplier);
            bb.setBandwidth(bb.getUpperBand() - bb.getLowerBand());
        }
        return bb;
    }

    /**
     * Returns the slope from closing stock prices based on the number of samples.  This is used to gauge overall
     * market conditions.  For example, an upward slope over a long period of time would indicate a bull market.
     *
     * @param stockPrices Stock price objects used to compare closing prices
     * @param samples The number of closing prices to use for the linear regression.  This starts from the end of the
     *                collection of StockPrices.  For example:  [1,2,3,4,5,6,7,8,9,10]  If the sample was set to 2, then
     *                [9,10] would be used for the linear regression
     * @return Slope of closing prices as a double
     */
    public static double getLinearRegressionSlope(List<StockPrice> stockPrices, int samples) {
        double slope = 0;
        double[][] datapoints = new double[samples][1];

        List<StockPrice> subList = stockPrices;
        if (samples < stockPrices.size()) {
            subList = stockPrices.subList(stockPrices.size()-samples-1, stockPrices.size()-1);
        }

        SimpleRegression regression = new SimpleRegression();
        for (int x=0; x<samples; x++) {
            regression.addData(x, stockPrices.get(x).getClosingPrice());
        }
        slope = regression.getSlope();
        return slope;
    }

    /**
     * Calculates historic volatility which essentially uses a SMA.
     *
     * @param stockPrices An array of StockPrice objects
     * @param idx The index of the 'stockPrices' of this value
     * @param period The period for the historic volatility
     * @param tradingDays Trading days to use for the volatility calculation. (252 for a year, etc.)
     * @return Volatilty
     */
    public static double getHistoricalVolatility(List<StockPrice> stockPrices, int idx, int period, int tradingDays) {
        if (period ==0) {
            period = 20;
        }
        if (tradingDays == 0 ) {
            tradingDays = 252;
        }

        if (idx >= period) { // User 'period', instead of (period -1) since the first calc is period+1
            List<Double> priceChanges = new ArrayList<Double>();
            double lastPrice = 0;
            for(int x=0; x<stockPrices.size(); x++) {
                if (x==0) {
                    priceChanges.add(0.0);
                    lastPrice = stockPrices.get(x).getClosingPrice();
                } else {
                    double priceChange = Math.log(stockPrices.get(x).getClosingPrice()/lastPrice);
                    priceChanges.add(priceChange);
                    lastPrice = stockPrices.get(x).getClosingPrice();
                }
            }
            // Grab an array of price changes over the selected period
            int tmp_price_changes_st = idx - (period -1);
            int tmp_price_changes_end = tmp_price_changes_st + period;
            List<Double> tmp_price_changes = priceChanges.subList(tmp_price_changes_st, tmp_price_changes_end);
            Double[] priceChangesArr = tmp_price_changes.toArray(new Double[0]);
            StandardDeviation stdDev = new StandardDeviation();
            double stdDevTmp = stdDev.evaluate(ArrayUtils.toPrimitive(priceChangesArr));
            double retTemp = stdDevTmp * Math.sqrt(tradingDays);
            return (double)retTemp;
        } else {
            return 0.0f;
        }
    }

    /**
     * Exponential weighted moving average (EWMA) volatility
     * This calculation was derived from a spreadsheet at Bionic Turtle
     * (http://www.bionicturtle.com/)
     *
     * @param stockPrices An array of StockPrice objects
     * @param idx The index of the 'stock_prices_array' where this calculation applies
     * @param tradingDays The # of days used in the calculation (252 for annualized volatility)
     * @param lambdaConst The lambda value used for the calculation. Defaults to the Risk Metrics value of 94%
     * @param arraySize The number of days to go back in the history of prices to make this calculation.
     * @return Volatility of the exponentially weighted moving average in decimal form. Multiply by 100 to get a percentage.
     */
    public static double getEWMAVolatility(List<StockPrice> stockPrices, int idx, int tradingDays,
                                           double lambdaConst, int arraySize) {
        int adjArraySize = arraySize;
        if (stockPrices.size() < arraySize)
            adjArraySize = stockPrices.size();

        List<Double> periodReturns = new ArrayList<Double>();
        List<Double> periodReturnsSquared = new ArrayList<Double>(); // Returns squared, which is the same as simple volatility

        // Create a subset of the prices based on the arraySize
        int tmpPriceSt = idx - (adjArraySize - 1);
        List<StockPrice> tmpStockPrices = stockPrices.subList(tmpPriceSt, adjArraySize);

        double lastPrice = 0;
        for (int x = 0; x < tmpStockPrices.size(); x++) {
            if (x == 0) {
                periodReturns.add(new Double(0));
                periodReturnsSquared.add(new Double(0));
                lastPrice = tmpStockPrices.get(x).getClosingPrice();
            } else {
                double priceChange = Math.log(tmpStockPrices.get(x).getClosingPrice() / lastPrice);
                periodReturns.add(priceChange);
                periodReturnsSquared.add(Math.pow(priceChange, 2));
                lastPrice = tmpStockPrices.get(x).getClosingPrice();
            }
        }

        List<Double> weightVars = new ArrayList<Double>(); // Weighted variances for day 'N'
        List<Double> weightVarsPrev = new ArrayList<Double>(); // Weighted variances for day 'N-1'

        double weight = 0; // The weight for day 'N'
        double weightPrev = 0; // The weight for day 'N-1'

        // Use a reverse loop to run through the prices from most recent to oldest
        for (int i = tmpStockPrices.size(); i > 0; i-- ) {
            if (i == (tmpStockPrices.size() - 2)) { // Weight for day 'N'
                weight = 1.0 - lambdaConst;
                weightVars.add(periodReturnsSquared.get(i) * weight);
            } else if (i == (tmpStockPrices.size() - 3)) { // Weight for day 'N-1'
                weightPrev = 1.0 - lambdaConst;
                weight = weight * lambdaConst;
                weightVars.add(periodReturnsSquared.get(i) * weight);
                weightVarsPrev.add(periodReturnsSquared.get(i) * weightPrev);
            } else if (i < tmpStockPrices.size() - 3) {
                weight = weight * lambdaConst;
                weightPrev = weightPrev * lambdaConst;
                weightVars.add(periodReturnsSquared.get(i) * weight);
                weightVarsPrev.add(periodReturnsSquared.get(i) * weightPrev);
            }
        }
        double sumVars = 0;
        for (int x=0; x<weightVars.size(); x++) {
            sumVars = sumVars + weightVars.get(x);
        }
        double sumVarsPrev = 0;
        for (int x=0; x<weightVarsPrev.size(); x++) {
            sumVarsPrev = sumVarsPrev + weightVarsPrev.get(x);
        }
        double ewma = lambdaConst * sumVarsPrev + (1 - lambdaConst) * periodReturnsSquared.get(adjArraySize - 2);
        return Math.sqrt(ewma) * Math.sqrt(tradingDays);
    }

    /**
     * Calculates volatility using the GARCH(1,1) formula.
     * This calculation was derived from a spreadsheet at Bionic Turtle
     * (http://www.bionicturtle.com/)
     * (NOTE: Gamma is determined using:  1 - alpha - beta)
     *
     * @param stockPrices An array of StockPrice objects
     * @param idx The index of the 'StockPrices' where this calculation applies
     * @param longRunVariance The long run variance for this series of prices
     * @param alpha The Alpha weight used for this calculation
     * @param beta The Beta weight used for this calculation
     * @param lambdaConst The lambda value used for the calculation. Defaults to the Risk Metrics value of 94%
     * @param tradingDays The # of days used in the calculation (252 for annualized volatility)
     * @param arraySize The number of days to go back in the history of prices to make this calculation.
     * @return Volatility of the GARCH(1,1) formula in decimal form. Multiply by 100 to get a percentage.
     * @throws Exception if Alpha + beta + Gamma are greater than 1  # TODO use a more specific exception
     */
    public static double getGarch1_1Volatility(List<StockPrice> stockPrices, int idx, double longRunVariance,
            double alpha, double beta, double lambdaConst, int tradingDays, int arraySize) throws Exception
    {
        int adjArraySize = arraySize;
        if (stockPrices.size() < arraySize)
            adjArraySize = stockPrices.size();

        // The three weights Alpha + Beta + Gamma can not be greater than 1
        // Throw an exception if this occurs.
        if (alpha + beta > 1.0)
            throw new Exception("The weights Alpha + Beta + Gamma should not be greater than 1");
        double gamma = 1 - alpha - beta;

        List<Double> periodReturns = new ArrayList<Double>();
        List<Double> periodReturnsSquared =new ArrayList<Double>(); // Returns squared, which is the same as simple volatility

        // Create a subset of the prices based on the arraySize
        int tmpPriceSt = idx - (adjArraySize - 1);
        List<StockPrice> tmpStockPrices = stockPrices.subList(tmpPriceSt, adjArraySize);

        double lastPrice = 0;
        for (int x = 0; x < tmpStockPrices.size(); x++) {
            if (x == 0) {
                periodReturns.add(new Double(0));
                periodReturnsSquared.add(new Double(0));
                lastPrice = tmpStockPrices.get(x).getClosingPrice();
            } else {
                double priceChange = Math.log(tmpStockPrices.get(x).getClosingPrice() / lastPrice);
                periodReturns.add(priceChange);
                periodReturnsSquared.add(Math.pow(priceChange, 2));
                lastPrice = tmpStockPrices.get(x).getClosingPrice();
            }
        }

        List<Double> weightVars = new ArrayList<Double>(); // Weighted variances for day 'N'
        List<Double> weightVarsPrev = new ArrayList<Double>(); // Weighted variances for day 'N-1'

        double weight = 0; // The weight for day 'N'
        double weightPrev = 0; // The weight for day 'N-1'

        // Use a reverse loop to run through the prices from most recent to oldest
        for (int i = tmpStockPrices.size(); i > 0; i--) {
            if (i == (tmpStockPrices.size() - 2)) { // Weight for day 'N'
                weight = 1.0 - lambdaConst;
                weightVars.add(periodReturnsSquared.get(i) * weight);
            } else if (i == (tmpStockPrices.size() - 3)) { // Weight for day 'N-1'
                weightPrev = 1.0 - lambdaConst;
                weight = weight * lambdaConst;
                weightVars.add(periodReturnsSquared.get(i) * weight);
                weightVarsPrev.add(periodReturnsSquared.get(i) * weightPrev);
            }
            else if (i < tmpStockPrices.size() - 3) {
                weight = weight * lambdaConst;
                weightPrev = weightPrev * lambdaConst;
                weightVars.add(periodReturnsSquared.get(i) * weight);
                weightVarsPrev.add(periodReturnsSquared.get(i) * weightPrev);
            }
        }
        double sumVars = 0;
        for (int x=0; x<weightVars.size(); x++) {
            sumVars = sumVars + weightVars.get(x);
        }
        double sumVarsPrev = 0;
        for (int x=0; x<weightVarsPrev.size(); x++) {
            sumVarsPrev = sumVarsPrev + weightVarsPrev.get(x);
        }
        double garch = (longRunVariance * gamma) + (alpha *
                periodReturnsSquared.get(adjArraySize - 2)) + (beta * sumVarsPrev);
        return Math.sqrt(garch) * Math.sqrt(tradingDays);
    }

    /**
     * Convenience method used for oscillator EMA calculations
     * @param prices
     * @return
     */
    private static List<MomentumVO> preprocessMACDPPOEMA(List<StockPrice> prices) {

        double prevEma12 = 0;
        double prevEma26 = 0;
        List<MomentumVO> stocksVO = new ArrayList<MomentumVO>();
        int x=0;
        for (StockPrice price : prices) {
            MomentumVO vo = new MomentumVO();
            vo.setId(price.getId());
            vo.setSymbol(price.getSymbol());
            vo.setOpeningPrice(price.getOpeningPrice());
            vo.setClosingPrice(price.getClosingPrice());
            vo.setAdjClosingPrice(price.getAdjClosingPrice());
            vo.setLowPrice(price.getLowPrice());
            vo.setHighPrice(price.getHighPrice());
            vo.setPriceDate(price.getPriceDate());
            vo.setPeriod(price.getPeriod());

            vo.setEma_12(TechnicalIndicators.getEMA(prices, x, 12, prevEma12));
            prevEma12 = vo.getEma_12();
            vo.setEma_26(TechnicalIndicators.getEMA(prices, x, 26, prevEma26));
            prevEma26 = vo.getEma_26();

            if (x + 1 >= 26)
                vo.setMacd_12_26(TechnicalIndicators.getMACD(vo.getEma_12(), vo.getEma_26()));
            else {
                vo.setMacd_12_26(0);
            }

            if (x + 1 >= 26)
                vo.setPpo_12_26(TechnicalIndicators.getPPO(vo.getEma_12(), vo.getEma_26()));
            else {
                vo.setPpo_12_26(0);
            }

            stocksVO.add(vo);
            x++;
        }
        return stocksVO;
    }

    private static double getMaxValue(double[] numbers) {
        double maxValue = numbers[0];
        for(int i=1;i < numbers.length;i++){
            if(numbers[i] > maxValue){
                maxValue = numbers[i];
            }
        }
        return maxValue;
    }

    private static double getMinValue(double[] numbers){
        double minValue = numbers[0];
        for(int i=1;i<numbers.length;i++){
            if(numbers[i] < minValue){
                minValue = numbers[i];
            }
        }
        return minValue;
    }

    private final static class MaxHighComparator implements Comparator<StockPrice>
    {
        @Override
        public int compare(StockPrice o1, StockPrice o2) {

            return new Double(o1.getHighPrice()).compareTo(new Double(o2.getHighPrice()));
        }
    }

    private final static class MinLowComparator implements Comparator<StockPrice>
    {
        @Override
        public int compare(StockPrice o1, StockPrice o2) {

            return new Double(o1.getLowPrice()).compareTo(new Double(o2.getLowPrice()));
        }
    }
}
