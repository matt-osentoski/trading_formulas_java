# Usage in Java applications:
```xml
<dependencies>
    <dependency>
        <groupId>com.garagebandhedgefund.trading.formulas</groupId>
        <artifactId>trading-formulas</artifactId>
        <version>1.1.0</version>
    </dependency>
</dependencies>

<repositories>
    <repository>
        <id>mosentos-repo</id>
        <url>https://bitbucket.org/mosentos/maven-repo/raw/master/maven-repo/</url>
    </repository>
</repositories>
```



#Building this Jar
To build a Jar so that it can be used in a remote repo, perform the following steps:

1. Run the following maven command against your JAR
mvn -DcreateChecksum=true clean install

2. Locate the directory structure under your ~.M2 directory on your local machine

3. Rename the 'maven-metadata-local.xml' files.  Remove the 'local' from them.

4. Upload your directory structure up to Bitbucket, etc.




