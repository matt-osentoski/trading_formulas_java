#Changelog
All notable changes to this project will be documented in this file.

The format is a subset of [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project uses parts of [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

##2.6.2
### Added
- Money management unit tests
### Changed
- Money management formula to fix the returned results.

##2.6.1
### Added
- Money management formulas.

##2.5.1
### Added
- Linear regression against prices.  This is to determine overall market conditions (bull, bear, etc.)

##2.4.1
### Added
- Bollinger Bands as a technical indicator.

##2.3.1
### Added
- The is_index column for stocks that are indexes. (ex: NASDAQ's ^IXIC)

##2.3.0
### Added
- Created a new class for Stock quotes.  This is lighter-weight than the full Stock object. This object should
be used for intraday stock quote pricing.  For now, it's assumed that only open ,high, low, and close (OHLC)
prices will be persisted.  The Stock Quote will not be an entity, at this point.

##2.2.0
### Removed
- All traces of Avro from this project.  Using a code first approach with Avro was proving
problematic.  I'm going to a Schema first approach now.  Getting dates to work with Json encoding via Avro
Unions was taking too much time, with the code-first approach.

##2.1.1
### Changed
- The Avro date annotations to use long epoch times for marshalling

##2.1.0
### Changed
- Switched from Jackson to the native Avro library for POJO to .avsc marshalling.
### Removed
- The Jackson library as a dependency.

##2.0.0
### Added
- All model objects now include avro Schema files at the root of the Jar file.
- Adding ETF related columns to the Stock entity

##1.9.0
### Added
- Price change formulas

##1.8.0
### Added
- dividend_per_share column.

##1.7.1
### Modified
- Fixed a typo in the preferred_equity column

##1.7.0
### Added
- The StockFundamentalData model object

##1.6.0
### Modified
- The 'id' in StockPrices from a string to an integer

##1.5.0
### Added
- A foreign key to stock_prices and changed the relationship from the natural key, 'symbol' to the
'id' from the stocks table.

##1.4.12
### Added
- JPA entity annotations on all domain objects.
### Removed
- The orm.xml file, since it is no longer needed

##1.3.0
### Added
- JPA mapping files (persistence.xml / orm.xml)
- An .gitignore file with intelliJ, eclipse, and Maven exclusions

##1.2.2
### Modified
- Fixed a bug in the marketCap setter in the Stock object

##1.2.1
### Modified
- Changed the type of marketCap in the Stock object from long to double

##1.2.0
### Removed
- Cleaned up the Stock object to reflect new properties provided by Nasdaq
- Older metadata property from the Stock object.

##1.1.0
### Added
- The 'Treasury' model
### Modified
- Changed the 'id' values to Strings to accomodate different data stores.

##1.0.0
### Added
- First release



